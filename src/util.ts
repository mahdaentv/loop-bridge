import * as _ from 'lodash';

export function diff(nextRec: any, prevRec: any) {
  return getChanges(nextRec, prevRec);
}

function getChanges(next: any, prev: any) {
  return _.transform(next, (result, value, key) => {
    if (!_.isEqual(value, prev[key])) {
      if (_.isObject(value) && _.isObject(prev[key])) {
        result[key] = getChanges(value, prev[key]);
      } else {
        result[key] = value;
      }
    }
  });
}
