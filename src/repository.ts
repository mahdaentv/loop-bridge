import {
  EntityData,
  EntityPart,
  RepositoryConfigs,
  RepositoryRelationRef,
  RequestFilter,
  RequestOptions
} from './interfaces';
import { DataSource } from './datasource';
import { Entities, Entity, EntityConstructor } from './entity';
import { AxiosResponse } from 'axios';
import { EventEmitter } from './event';
import { Query } from './query';
import { SchemaDefinitions, SchemaError, SchemaErrorContext } from './schema';
import {
  SubscriptionHandler,
  Subscription
} from 'event-bridge/dist/client';

declare var toString: any;

/**
 * Repository is a class helper of a Model endpoint. For example, if we have a `/users` endpoint,
 * we can create a `Users` repository so we can easily perform CRUD requests without manually
 * define the request path for each requests.
 *
 * @typeparam T Data interface that extends [[EntityData]]
 *
 * @example
 * ```typescript
 * interface User extends EntityData {
 *   firstName: string;
 *   lastName: string;
 * }
 *
 * const datasource = new DataSource({ name:'main', baseURL: 'http://localhost:3000' });
 * class Users extends Repository<User> {}
 * const users = new Users(datasource, { name: 'user', endpoint: '/users' });
 *
 * const newUser = await users.create({ firstName: 'John', lastName: 'Doe' });
 * await newUser.update({ firstName: 'Johnny' });
 * ```
 */
export class Repository<T extends EntityData> {
  /** Schema definitions to validate the data. **/
  private readonly schemas: SchemaDefinitions;
  /** List of registered named queries. **/
  private queries: {
    [key: string]: Query<T>;
  } = {};
  /** An event emitter that will emit [[RequestEvent]] after the request finished. **/
  public events: EventEmitter<T> = new EventEmitter<T>();

  /** A property to define custom Entity class instead using the default one **/
  protected EntityClass?: EntityConstructor<T> = Entity;

  /** Getter to return the Repository endpoint. **/
  protected get endpoint() {
    return this.configs.endpoint;
  }

  /**
   * Create a Repository instance.
   * @param datasource A [[DataSource]] instance so the new repository instance will be automatically
   * retistered to the [[DataSource]].
   * @param configs Repository configs to define the required configs.
   */
  constructor(public datasource: DataSource, public configs: RepositoryConfigs) {
    this.datasource.addRepository<Repository<T>>(this);

    if (configs.schemas) {
      this.schemas = configs.schemas;
    }
  }

  /**
   * Create a new named [[Query]], or return the existing one. This method is useful to manage
   * a shared listing so we don't need re-request the data for the same listing, even if on the different
   * page. If the param `name` is undefined, then the method will always returns new [[Query]].
   * @param name Query name or initial filter for non named filter.
   * @param filter Initial filter to be set.
   */
  public query(filter?: RequestFilter<T>, options?: RequestOptions<T>): Query<T>;
  public query(name?: string, filter?: RequestFilter<T>, options?: RequestOptions<T>): Query<T>;
  public query(name?: string | RequestFilter<T>, filter?: RequestFilter<T> | RequestOptions<T>, options?: RequestOptions<T>): Query<T> {
    if (typeof name === 'string') {
      if (!this.queries[name]) {
        this.queries[name] = new Query<T>(this, filter as RequestFilter<T>, options);
      }
      return this.queries[name];
    } else {
      return new Query(this, name, options);
    }
  }

  /**
   * Perform a `GET /{ENDPOINT}/count` request.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * users.count().then(data => console.log(`There are ${data.count} users.`));
   * ```
   */
  public async count(options: RequestOptions<T> = {}): Promise<number> {
    try {
      this.prefixRelations(options);
      const response = await this.datasource.get(`${this.endpoint}/count`, options);
      return parseFloat(response.data.count);
    } catch (error) {
      return 0;
    }
  }

  /**
   * Perform a `GET /{ENDPOINT}` request. The response will be an [[Entities]].
   * @param options Request options.
   *
   * @example
   * ```typescript
   * users.find().then(console.log); // [ ...Entity<User> ]
   * ```
   */
  public async find(options: RequestOptions<T> = {}): Promise<Entities<T>> {
    try {
      if (this.beforeFind) await this.beforeFind(options);

      this.prefixRelations(options);
      this.injectHeaders(options);

      const response = await this.datasource.get(this.endpoint, options);
      if (this.transformFindResponse) this.transformFindResponse(response);
      const entities = this.createEntity(response.data as T[], options);

      if (options && (options.withChildren || options.recursive)) {
        await this.injectRelations(entities, options.recursive);
      }

      if (this.afterFind) await this.afterFind(entities, response);
      this.events.emit({ type: 'find', options, entities, response });

      return entities;
    } catch (error) {
      if (this.failedFind) await this.failedFind(error, options);
      throw error;
    }
  }

  /**
   * Peform a `GET /{ENDPOINT}/{ID}` request. The reponse will be an object.
   * @param id Entity ID.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * users.findOne('1111').then(user => console.log(user.data.firstName)); // John Doe
   * ```
   */
  public async findOne(id: string, options: RequestOptions<T> = {}): Promise<Entity<T>> {
    try {
      if (this.beforeFindOne) await this.beforeFindOne(id, options);

      this.prefixRelations(options);
      this.injectHeaders(options);

      const response = await this.datasource.get(`${this.endpoint}/${id}`, options);
      if (this.transformFindOneResponse) this.transformFindOneResponse(response);
      const entity = this.createEntity(response.data as T, options);

      if (options && (options.withChildren || options.recursive)) {
        await this.injectRelations(entity, options.recursive);
      }

      if (this.afterFindOne) await this.afterFindOne(entity, response);
      this.events.emit({ type: 'find-one', id, options, entity, response });

      return entity;
    } catch (error) {
      if (this.failedFindOne) await this.failedFindOne(error, id, options);
      throw error;
    }
  }

  /**
   * Perform a `POST /{ENDPOINT}` request. The response will be an Entity.
   * @param body Request body.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * users.create({ firstName: 'John', lastName: 'Doe' }).then(console.log); // Entity { firstName, lastName }
   * ```
   */
  public async create(body: T, options: RequestOptions<T> = {}): Promise<Entity<T>> {
    try {
      if (this.beforeCreate) await this.beforeCreate(body, options);

      this.excludeFields(body, options);
      this.prefixRelations(options, body);
      this.ensureSchemas(body);
      this.injectHeaders(options);

      const response = await this.datasource.post(this.endpoint, body, options);
      if (this.transformCreateResponse) this.transformCreateResponse(response);
      const entity = this.createEntity(response.data, options);

      if (this.afterCreate) await this.afterCreate(entity, response);
      this.events.emit({ type: 'create', options, entity, response });

      return entity;
    } catch (error) {
      if (this.failedCreate) await this.failedCreate(error, body, options);
      throw error;
    }
  }

  /**
   * Pefrom a `PATCH /{ENDPOINT}/{ID}` request. The response will be nothing.
   * @param id Entity ID.
   * @param body Request body.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * users.update('1111', { firstName: 'Johnny' }).then(console.log);
   * ```
   */
  public async update(id: string, body: EntityPart<T>, options: RequestOptions<T> = {}): Promise<void> {
    try {
      if (this.beforeUpdate) await this.beforeUpdate(id, body, options);

      this.excludeFields(body, options);
      this.injectHeaders(options);
      const response = await this.datasource.patch(`${this.endpoint}/${id}`, body, options);
      if (this.transformUpdateResponse) this.transformUpdateResponse(response);

      if (this.afterUpdate) await this.afterUpdate(response);
      this.events.emit({ type: 'update', id, options, response });
    } catch (error) {
      if (this.failedUpdate) await this.failedUpdate(error, id, body, options);
      throw error;
    }
  }

  /**
   * Peform a bulk `PATCH` requests. The response will be nothing.
   * @param entities Entity list.
   * @param body Request body.
   * @param options Request options.
   */
  public async updateAll(entities: Entities<T> | Entity<T>[], body: EntityPart<T>, options: RequestOptions<T> = {}): Promise<void> {
    try {
      this.excludeFields(body, options);
      this.ensureSchemas(body);
      this.injectHeaders(options);

      if (this.configs.hasBulkOperations) {
        const ids = entities.map(entity => entity.id);
        const response = await this.datasource.patch(`${this.endpoint}?entityIds=${ids.toString()}`, body, options);
        entities.forEach(entity => entity.assign(body));
        this.events.emit({ type: 'update-all', options, response });
      } else {
        const responses = await Promise.all(entities.map(entity => entity.update(body, options)));
        this.events.emit({ type: 'update-all', options, data: responses });
      }
    } catch (error) {
      throw error;
    }
  }

  /**
   * Perform a `PUT /{ENDPOINT}/{ID}` request. The response will be nothing.
   * @param id Entity ID.
   * @param body Request body.
   * @param options Request options.
   */
  public async replace(id: string, body: T, options: RequestOptions<T> = {}): Promise<void> {
    try {
      if (this.beforeReplace) await this.beforeReplace(id, body, options);

      this.excludeFields(body, options);
      this.ensureSchemas(body);
      this.injectHeaders(options);
      const response = await this.datasource.put(`${this.endpoint}/${id}`, body, options);
      if (this.transformReplaceResponse) this.transformReplaceResponse(response);

      if (this.afterReplace) await this.afterReplace(response);
      this.events.emit({ type: 'replace', id, options, response });
    } catch (error) {
      if (this.failedReplace) await this.failedReplace(error, id, body, options);
      throw error;
    }
  }

  /**
   * Pefrom a `DELETE /{ENDPOINT}/{ID}` request. The response will be nothing.
   * @param id Entity ID.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * users.delete('1111').then(console.log);
   * ```
   */
  public async delete(id: string, options: RequestOptions<T> = {}): Promise<void> {
    try {
      if (this.beforeDelete) await this.beforeDelete(id, options);

      this.injectHeaders(options);
      const response = await this.datasource.delete(`${this.endpoint}/${id}`, options);
      if (this.transformDeleteResponse) this.transformDeleteResponse(response);

      if (this.afterDelete) await this.afterDelete(response);
      this.events.emit({ type: 'delete', id, options, response });
    } catch (error) {
      if (this.failedDelete) await this.failedDelete(error, id, options);
      throw error;
    }
  }

  /**
   * Perform a bulk `DELETE` requests. The response will be nothing.
   * @param entities Entity list.
   * @param options Request options.
   */
  public async deleteAll(entities: Entities<T> | Entity<T>[], options: RequestOptions<T> = {}): Promise<void> {
    try {
      this.injectHeaders(options);

      if (this.configs.hasBulkOperations) {
        const ids = entities.map(entity => entity.id);
        const response = await this.datasource.delete(`${this.endpoint}?entityIds=${ids.toString()}`, options);
        entities.forEach(entity => entity.assign({ deletedAt: new Date() } as any));
        this.events.emit({ type: 'delete-all', options, response });
      } else {
        const responses = await Promise.all(entities.map(entity => entity.delete(options)));
        this.events.emit({ type: 'delete-all', options, data: responses });
      }
    } catch (error) {
      throw error;
    }
  }

  /**
   * Subscribe to the `find()` endpoint or `findOne()` endpoint, so the subscriber will get an updates
   * when there is a `POST`, `PUT`, `PATCH`, or `DELETE` event happend on the endpoint.
   * @param id Entity ID to subscribe to `findOne()`, or function to handle the event to subscribe to `find()`.
   * @param handler Function to handle the event to subscribe to `findOne()`, request options to subscribe to `find()`.
   * @param options Request options, applicable to subscribe to `findOne()`.
   */
  public async subscribe(id: string, handler: SubscriptionHandler<T>, options?: RequestOptions<T>): Promise<Subscription<T>>;
  public async subscribe(handler: SubscriptionHandler<T>, options?: RequestOptions<T>): Promise<Subscription<T>>;
  public async subscribe(
    idHandler: string | SubscriptionHandler<T>,
    optionsHandler?: SubscriptionHandler<T> | RequestOptions<T>,
    options?: RequestOptions<T>
  ): Promise<Subscription<T>> {
    if (typeof idHandler === 'string') {
      await this.mergeEndpoint(options);
      await this.injectHeaders(options);
      return await this.datasource.subscribe(`${this.endpoint}/${idHandler}`, optionsHandler as SubscriptionHandler<T>, options);
    } else {
      await this.mergeEndpoint(optionsHandler as RequestOptions<T>);
      await this.injectHeaders(optionsHandler as RequestOptions<T>);
      return await this.datasource.subscribe<T>(this.endpoint, idHandler, optionsHandler as RequestOptions<T>);
    }
  }

  /**
   * Internal method to exclude specific fields from the request body.
   * @param body Request body.
   * @param options Requst options.
   */
  protected excludeFields(body: T | EntityPart<T>, options?: RequestOptions<T>): void {
    if (Array.isArray(options.excludeFields)) {
      for (const key of options.excludeFields) {
        delete body[key];
      }
    }
  }

  /**
   * Create new Entities or Entity from an array or object. Beside used as internal method, you can
   * use this method to create a blank entity to use it in a form component.
   * @param data Array or Object to be created.
   *
   * @example
   * ```typescript
   * users.createEntity({ firstName: 'John', lastName: 'Doe' }); // Entity { firstName, lastName }
   * users.createEntity([ { firstName: 'John', lastName: 'Doe' } ]); // Entities [ ...{ firstName, lastName } ]
   * ```
   */
  public createEntity(data?: T, options?: RequestOptions<T>): Entity<T>;
  public createEntity(data?: T[], options?: RequestOptions<T>): Entities<T>;
  public createEntity(data?: T | T[], options?: RequestOptions<T>): Entity<T> | Entities<T> {
    if (!data) {
      return new this.EntityClass(this, {} as any);
    }

    if (Array.isArray(data)) {
      const entities = new Entities<T>(this, ...data.map(item => {
        if (item) {
          this.ensureSchemas(item, true);
        }
        return new this.EntityClass(this, item, options);
      }));
      if (data.hasOwnProperty('meta')) {
        entities.meta = (data as any).meta;
      }
      return entities;
    } else {
      if (data) {
        this.ensureSchemas(data, true);
      }

      return new this.EntityClass(this, data, options);
    }
  }

  /**
   * Validate data with the given schemas. This method expected to throw an error if the given data
   * does not meet with the schema, and fill the data with the default values if not exist.
   * @param data Data to be validated.
   */
  public ensureSchemas(data: EntityPart<T>, remote?: boolean): void {
    if (this.schemas) {
      const errContext: SchemaErrorContext = {};
      for (const [key, definition] of Object.entries(this.schemas)) {
        if (data.hasOwnProperty(key)) {
          const givenType = toString.call(data[key])
            .replace('[object ', '')
            .replace(']', '')
            .toLowerCase();

          if (definition.type !== givenType) {
            errContext[key] = { expected: definition.type, given: data[key] }
          }
        } else {
          if (definition.default) {
            if (typeof definition.default === 'function') {
              data[key] = definition.default(key);
            } else {
              data[key] = definition.default;
            }
          } else {
            if (remote) {
              if (definition.required) {
                errContext[key] = { expected: definition.type, given: undefined };
              }
            } else {
              if (definition.required && !definition.generated) {
                errContext[key] = { expected: definition.type, given: undefined };
              }
            }
          }
        }
      }

      if (Object.keys(errContext).length) {
        throw new SchemaError(errContext);
      }
    }
  }

  /**
   * Change the Entity class to be used when creating new Entity.
   * @param type Custom class that extends Entity.
   *
   * @example
   * ```typescript
   * class UserEntity extends Entity<User> {}
   *
   * users.useClass(UserEntity);
   * users.createEntity({ firstName: 'John', lastName: 'Doe' }); // UserEntity { firstName, lastName }
   * ```
   */
  public useClass<T extends Entity<any>>(type: any): void {
    this.EntityClass = type;
  }

  /**
   * Internal method used to manage the repository `hasMany` and `hasOne` relations before returning the data after requests.
   * @param data Entity to manage the relations.
   * @param recursive Recursive will fetch all the relations recursively.
   */
  protected async injectRelations(data: Entity<T>, recursive?: boolean): Promise<Entity<T>>;
  protected async injectRelations(data: Entities<T>, recursive?: boolean): Promise<Entities<T>>;
  protected async injectRelations(data: Entity<T> | Entities<T>, recursive?: boolean): Promise<Entity<T> | Entities<T>> {
    if (data instanceof Entities) {
      await Promise.all(data.map(item => this.injectRelations(data)));
    } else {
      const promises = [];
      const { relations } = this.configs;

      if (relations && relations.hasMany) {
        for (const rel of relations.hasMany) {
          const instance = this.datasource.getRepository<Repository<any>>(rel.repository);
          if (instance) {
            promises.push(new Promise(async (resolve, reject) => {
              try {
                const child = await instance.find({
                  params: { [rel.foreignKey]: data['id'] },
                  recursive
                });
                data[rel.localField] = child;
                resolve(data);
              } catch (error) {
                reject(error);
              }
            }));
          }
        }
      }

      if (relations && relations.hasOne) {
        for (const rel of relations.hasOne) {
          if (data[rel.foreignKey]) {
            const instance = this.datasource.getRepository<Repository<any>>(rel.repository);
            if (instance) {
              promises.push(new Promise(async (resolve, reject) => {
                try {
                  const child = await instance.findOne(data[rel.foreignKey], { recursive });
                  data[rel.localField] = child;
                  resolve(data);
                } catch (error) {
                  reject(error);
                }
              }));
            }
          }
        }
      }

      if (promises.length) {
        await Promise.all(promises);
      }
    }

    return data;
  }

  /**
   * Internal method to manage the `belongsTo` relations. This method will prefix the request with the
   * parent Repository endpoint.
   * @param options Request options.
   */
  protected prefixRelations(options: RequestOptions<T> = {}, body?: T): void {
    const relationRefs = this.parseRelations(options, body);

    if (relationRefs.length) {
      const prefixes = relationRefs.map(({ key, endpoint, value }) => {
        delete options.params[key];
        delete (body || {})[key];
        return [endpoint, value].join('/');
      });

      options.prefix = options.prefix ? `${prefixes.join('/')}/${options.prefix}` : prefixes.join('/');
    }

    this.mergeEndpoint(options);
  }

  /**
   * Parse relationships from the request.
   * @param options Request options.
   * @param body Request body.
   */
  protected parseRelations(options: RequestOptions<T> = {}, body?: T): RepositoryRelationRef[] {
    const { relations } = this.configs;
    const { params = {} } = options;

    if (relations && relations.belongsTo) {
      const relationRefs: RepositoryRelationRef[] = [];
      const relationParams: { [key: string]: string } = {};

      for (const rel of relations.belongsTo) {
        const { repository, foreignKey } = rel;
        const instance = this.datasource.getRepository(repository);

        if (instance) {
          const { endpoint } = instance.configs;

          if (body && body[foreignKey]) {
            relationRefs.push({
              key: foreignKey,
              value: body[foreignKey],
              endpoint
            });
            relationParams[foreignKey] = body[foreignKey];
          }

          if (params[foreignKey]) {
            relationRefs.push({
              key: foreignKey,
              value: params[foreignKey],
              endpoint
            });
            relationParams[foreignKey] = params[foreignKey];
          }
        }
      }

      if (relationRefs.length) {
        if (!options.params) {
          options.params = {};
        }

        options.params.relationRef = relationParams;
      }

      return relationRefs;
    }

    return [];
  }

  /**
   * Internal method to merge the `endpointPrefix` and `endpointSuffix` into the `endpoint`.
   * @param options Request options.
   */
  protected mergeEndpoint(options: RequestOptions<T> = {}): void {
    const { endpointPrefix, endpointSuffix } = this.configs;
    const { prefix, suffix } = options;

    if (endpointPrefix) {
      options.prefix = prefix ? `${endpointPrefix}/${prefix}` : endpointPrefix;
    }

    if (endpointSuffix) {
      options.suffix = suffix ? `${endpointSuffix}/${suffix}` : endpointSuffix;
    }
  }

  /**
   * Internal method to inject custom headers to the request options.
   * @param options Request options.
   */
  protected injectHeaders(options: RequestOptions<T> = {}): void {
    if (this.configs.headers) {
      options.headers = { ...this.configs.headers, ...options.headers || {} };
    }
  }

  /**
   * A request hook that will be invoked after the `.get()` request to manage the Axios response.
   * @param response Request response.
   */
  protected transformFindResponse?(response: AxiosResponse): Promise<void> | void;

  /**
   * A request hook that will be invoked after the `.get()` request to manage the Axios response.
   * @param response Request response.
   */
  protected transformFindOneResponse?(response: AxiosResponse): Promise<void> | void;

  /**
   * A request hook that will be invoked after the `.post()` request to manage the Axios response.
   * @param response Request response.
   */
  protected transformCreateResponse?(response: AxiosResponse): Promise<void> | void;

  /**
   * A request hook that will be invoked after the `.patch()` request to manage the Axios response.
   * @param response Request response.
   */
  protected transformUpdateResponse?(response: AxiosResponse): Promise<void> | void;

  /**
   * A request hook that will be invoked after the `.put()` request to manage the Axios response.
   * @param response Request response.
   */
  protected transformReplaceResponse?(response: AxiosResponse): Promise<void> | void;

  /**
   * A request hook that will be invoked after the `.delete()` request to manage the Axios response.
   * @param response Request response.
   */
  protected transformDeleteResponse?(response: AxiosResponse): Promise<void> | void;

  /**
   * A request hook that will be invoked before the `.find()` request, so we can modify the request options.
   * @param options Request options.
   */
  protected beforeFind?(options?: RequestOptions<T>): Promise<void> | void;

  /**
   * A request hook that will be invoked when an error occurs.
   * @param error Error object.
   * @param options Request options.
   */
  protected failedFind?(error: Error, options?: RequestOptions<T>): Promise<void> | void;

  /**
   * A request hook that will be invoked after the `.find()` request, so we can manage the responses.
   * @param entities Entities returned from the request.
   * @param response Raw Axios reponse object.
   */
  protected afterFind?(entities: Entity<T>[], response: AxiosResponse): Promise<void> | void;

  /**
   * A request hook that will be invoked before the `.findOne()` request.
   * @param id Entity ID.
   * @param options Request options.
   */
  protected beforeFindOne?(id: string, options?: RequestOptions<T>): Promise<void> | void;

  /**
   * A request hook that will be invoide when an error occurs.
   * @param error Error object.
   * @param id Entity ID.
   * @param options Request options.
   */
  protected failedFindOne?(error: Error, id: string, options?: RequestOptions<T>): Promise<void> | void;

  /**
   * A request hook that will be invoked after the `.findOne()` request.
   * @param entity Entity returned from the request.
   * @param response Raw Axios response object.
   */
  protected afterFindOne?(entity: Entity<T>, response: AxiosResponse): Promise<void> | void;

  /**
   * A request hook that will be invoked before the `.create()` request.
   * @param body Request body.
   * @param options Request options.
   */
  protected beforeCreate?(body: T, options?: RequestOptions<T>): Promise<void> | void;

  /**
   * A request hook that will be invoked when an error occurs.
   * @param error Error object.
   * @param body Request body.
   * @param options Request options.
   */
  protected failedCreate?(error: Error, body: T, options?: RequestOptions<T>): Promise<void> | void;

  /**
   * A request hook that will be invoked after the `.create()` request.
   * @param entity Entity returned from the request.
   * @param response Raw Axios response object.
   */
  protected afterCreate?(entity: Entity<T>, response: AxiosResponse): Promise<void> | void;

  /**
   * A request hook that will be invoked before the `.update()` request.
   * @param id Entity ID.
   * @param body Request body.
   * @param options Request options.
   */
  protected beforeUpdate?(id: string, body: any, options?: RequestOptions<T>): Promise<void> | void;

  /**
   * A request hook that will be invoked when an error occurs.
   * @param error Error object.
   * @param id Entity ID.
   * @param body Request body.
   * @param options Request options.
   */
  protected failedUpdate?(error: Error, id: string, body: any, options?: RequestOptions<T>): Promise<void> | void;

  /**
   * A request hook that will be invoked after the `.update()` request.
   * @param response Raw Axios response object.
   */
  protected afterUpdate?(response: AxiosResponse): Promise<void> | void;

  /**
   * A request hook that will be invoked before the `.replace()` request.
   * @param id Entity ID.
   * @param body Request body.
   * @param options Request options.
   */
  protected beforeReplace?(id: string, body: any, options?: RequestOptions<T>): Promise<void> | void;

  /**
   * A request hook that will be invoked when an error occurs.
   * @param error Error object.
   * @param id Entity ID.
   * @param body Request body.
   * @param options Request options.
   */
  protected failedReplace?(error: Error, id: string, body: any, options?: RequestOptions<T>): Promise<void> | void;

  /**
   * A request hook that will be invoked after the `.replace()` request.
   * @param response Raw Axios response object.
   */
  protected afterReplace?(response: AxiosResponse): Promise<void> | void;

  /**
   * A request hook that will be invoked before the `.delete()` request.
   * @param id Entity ID.
   * @param options Request options.
   */
  protected beforeDelete?(id: string, options?: RequestOptions<T>): Promise<void> | void;

  /**
   * A request hook that will be invoked when an error occurs.
   * @param error Error object.
   * @param id Entity ID.
   * @param options Request options.
   */
  protected failedDelete?(error: Error, id: string, options?: RequestOptions<T>): Promise<void> | void;

  /**
   * A request hook that will be invoked after the `.delete()` request.
   * @param response Raw Axios response object.
   */
  protected afterDelete?(response: AxiosResponse): Promise<void> | void;
}

/**
 * A class constructor that extends Repository.
 */
export type RepositoryConstructor<T extends EntityData> = { new(...args: any[]): Repository<T> };
