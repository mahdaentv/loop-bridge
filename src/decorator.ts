import { DataSource, DataSourceConstructor } from './datasource';
import { SchemaDefinitions } from './schema';
import { Repository, RepositoryConstructor } from './repository';
import { RequestFilter, RequestOptions } from './interfaces';

/**
 * [[DataSource]] collection to help the decorators manage DataSources and Repositories.
 */
export const dataSources: {
  /** Default [[DataSource]] to handle when no specific [[DataSource]] defined on the decorator call. **/
  default?: DataSource,
  /** List of registered DataSources. **/
  sources: {
    [key: string]: DataSource
  }
} = {
  default: null,
  sources: {}
};

/**
 * Assign a [[Repository]] instance to an object or class property.
 * @param repository [[Repository]] name, or [[Repository]] class, or [[DataSource]] name and [[Repository]] name with `:` delimiter.
 * @param datasource [[DataSource]] name, or [[DataSource]] class.
 *
 * @example
 * ```typescript
 * import { repository } from 'loop-bridge';
 * import { Users } from './user-service.ts';
 *
 * export class UserComponent {
 *   @repository(Users)
 *   users: Users;
 * }
 */
export function repository(repositoryName: string, dataSourceName?: string);
export function repository(repositoryName: string, DataSourceClass?: DataSourceConstructor);
export function repository(RepositoryClass: RepositoryConstructor<any>, dataSourceName?: string);
export function repository(RepositoryClass: RepositoryConstructor<any>, DataSourceClass?: DataSourceConstructor);
export function repository(repository: string | RepositoryConstructor<any>, datasource?: string | DataSourceConstructor) {
  if (typeof repository === 'string' && repository.includes(':')) {
    const [d, r] = repository.split(':');
    repository = r;
    datasource = d;
  }

  return function(constructor, key: string) {
    Object.defineProperty(constructor, key, {
      get: () => getRepository(repository as any, datasource as any)
    });
  }
}

/**
 * Assign a named [[Query]] to an object or class property.
 * @param name [[Repository]] name and [[Query]] name with `:` delimiter, or [[DataSource]] name, [[Repository]] name, and [[Query]] name with `:` delimiter.
 * @param filter Initial request filter.
 *
 * @example
 * ```typescript
 * import { Query, query }  from 'loop-bridge';
 * import { Users, User } from './user-service.ts';
 *
 * export class UserComponent {
 *   @query('user:list', { limit:10 })
 *   users: Query<User>;
 *
 *   constructor() {
 *     console.log(this.users.entities);
 *   }
 * }
 * ```
 */
export function query<T>(name: string, filter?: RequestFilter<T>, options?: RequestOptions<T>) {
  let repository: string, datasource: string;

  if (name.includes(':')) {
    const names = name.split(':');

    if (names.length === 3) {
      datasource = names[0];
      repository = names[1];
      name = names[2];
    } else {
      repository = names[0];
      name = names[1];
    }
  } else {
    throw new Error('Invalid query name format!');
  }

  if (!repository) {
    throw new Error(`Query need a repository name or type to be defined.`);
  }

  return function(constructor, key: string) {
    Object.defineProperty(constructor, key, {
      get: () => getRepository(repository, datasource).query(name, filter, options)
    });
  }
}

/**
 * Assign an empty [[Entity]] to a class property.
 * @param repository [[Repository]] name or [[Repository]] class.
 * @param data Initial data to be set.
 *
 * @example
 * ```typescript
 * export class UserComponent {
 *   @entity(Users)
 *   userForm: Entity<User>;
 * }
 */
export function entity(repositoryName: string, data?: any);
export function entity(RepositoryClass: RepositoryConstructor<any>, data?: any);
export function entity(repository: string | RepositoryConstructor<any>, data?: any) {
  return function(constructor, key: string) {
    const repo = getRepository(repository as any);
    constructor[key] = repo.createEntity(data);
  }
}

/**
 * Get a repository instance from the default [[DataSource]] or from the given [[DataSource]].
 * @param repository [[Repository]] name, or [[Repository]] class.
 * @param datasource [[DataSource]] name, or [[DataSource]] class. Used to get repository from a specific [[DataSource]].
 *
 * @example
 * ```typescript
 * const users = getRepository('user');
 * const externalUsers = getRepository('ext-user',  'external-api');
 * ```
 */
export function getRepository(repositoryName: string, dataSourceName?: string);
export function getRepository(repositoryName: string, DataSourceClass?: DataSourceConstructor);
export function getRepository(RepositoryClass: RepositoryConstructor<any>, dataSourceName?: string);
export function getRepository(RepositoryClass: RepositoryConstructor<any>, DataSourceClass?: DataSourceConstructor);
export function getRepository(repository: string | RepositoryConstructor<any>, datasource?: string | DataSourceConstructor) {
  let repo: Repository<any>;

  if (datasource) {
    if (typeof datasource === 'string') {
      if (dataSources.sources[datasource]) {
        repo = dataSources.sources[datasource].getRepository(repository);
      } else {
        throw new Error(`Expected DataSource ${datasource} does not exist!`);
      }
    } else {
      let ds: DataSource;
      for (const [, value] of Object.entries(dataSources.sources)) {
        if (value instanceof datasource) {
          ds = value;
        }
      }

      if (ds) {
        repo = ds.getRepository(repository);
      } else {
        throw new Error(`Expected DataSource ${datasource} does not exist!`);
      }
    }
  } else {
    if (dataSources.default) {
      repo = dataSources.default.getRepository(repository);
    } else {
      if (Object.keys(dataSources.sources).length) {
        for (const [, value] of Object.entries(dataSources.sources)) {
          if (!dataSources.default) {
            dataSources.default = value;
          }
        }

        repo = dataSources.default.getRepository(repository);
      } else {
        throw new Error(`Default DataSource does not exist!`);
      }
    }
  }

  if (repo) {
    return repo;
  } else {
    throw new Error(`Expected Repository ${repository} does not exist!`);
  }
}

/**
 * Define schema of a [[Repository]]. Schemas will be validated for incomming data, and outgoing `POST` and `PUT` data.
 * @param schemas Schema definitions.
 * @constructor
 *
 * @example
 * ```typescript
 * @Schema({
 *   firstName: {
 *     type: 'string',
 *     required: true
 *   },
 *   createdAt: {
 *     type: 'date',
 *     generated: true
 *   },
 *   verified: {
 *     type: 'boolean',
 *     default: (data: any) => typeof data.verifiedAt === 'date'
 *   }
 * })
 * export class Users extends Repository<User> {}
 * ```
 */
export function Schema(schemas: SchemaDefinitions) {
  return function(constructor: Function, key: string) {
    constructor.prototype.schemas = schemas;
  }
}

