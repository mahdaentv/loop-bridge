import {
  EntityData,
  EntityPart, FieldList,
  Fields,
  InclusionFilter,
  OrderFilter,
  QueryMetadata,
  RequestFilter,
  RequestOptions,
  RequestParams,
  WhereFilter
} from './interfaces';
import { Repository } from './repository';
import { Entities, Entity } from './entity';
import { EventEmitter, EventHandler, Unsubscriber } from './event';
import { Subscription } from 'event-bridge/dist/client';

/**
 * A class that wrap a listing request, so we can easily manage its filter and data.
 */
export class Query<T extends EntityData> {
  private subscription: Subscription<T>;
  public events = new EventEmitter();
  /** Entities returned from the request, and will be replaced by the `.fetch()` call. **/
  public entities: Entities<T>;
  /** Property contains the query parameters. **/
  public params: RequestParams;
  /** Property contains the query metadata. **/
  public meta: QueryMetadata;
  /** The query status to help displaying the loading bar **/
  public state: 'loading' | 'load-more' | 'complete';

  /** Return the selected entities, marked by Entity.selected = true; **/
  public get selectedEntities(): Entity<T>[] {
    return this.entities.selected || [];
  }

  /** Tell does some entities are selected. **/
  public get fewEntitiesSelected(): boolean {
    return this.entities.fewSelected;
  }

  /** Tell does all entities are selected. **/
  public get allEntitiesSelected(): boolean {
    return this.entities.allSelected;
  }

  /** Tell does the `.prev()` method is callable or not. **/
  public get prevEnabled(): boolean {
    return this.meta.page > 1;
  }

  /** Tell does the `.next()` method is callable or not. **/
  public get nextEnabled(): boolean {
    return this.meta.page < this.meta.totalPages;
  }

  /**
   * Create new Query instance to manage the data.
   * @param repository Repository instance to manage the request.
   * @param filter Initial filter to be applied.
   * @param options Initial request options to be applied.
   */
  constructor(private repository: Repository<T>, public filter: RequestFilter<T> = {}, public options: RequestOptions<T> = {}) {
    Object.assign(this, { entities: [] });
    this.reset(filter);
  }

  /**
   * Reset the Query to the initial state.
   */
  public reset(filter: RequestFilter<T> = {}): this {
    this.filter = filter;
    this.params = {};
    this.meta = { page: 1, totalPages: 1, total: 0 };

    // Using default repository limit if any.
    if (this.repository.configs.defaultLimit) {
      this.limit(this.repository.configs.defaultLimit);
    }

    // Set the limit if not defined by user or repository config.
    if (!this.filter.limit) {
      this.filter.limit = 25;
    }

    return this;
  }

  /**
   * Assign set of filters to the existing filter.
   * @param filter Request filter.
   */
  public assign(filter: RequestFilter<T>): this {
    Object.assign(this.filter, filter);
    return this;
  }

  /**
   * Set the limit of the listing request.
   * @param limit Request limit.
   * @param fetch Define to auto fetch the data or not.
   * @param options Request options, used when the fetch is true.
   */
  public limit(limit: number, fetch?: boolean, options?: RequestOptions<T>): this {
    this.filter.limit = limit;

    if (fetch) {
      this.fetch(options).then(() => {}).catch(() => {});
    }
    return this;
  }

  /**
   * Set the offset of the listing request.
   * @param offset Request offset.
   * @param fetch Define to auto fetch the data or not.
   * @param options Request options, used when the fetch is true.
   */
  public offset(offset: number, fetch?: boolean, options?: RequestOptions<T>): this {
    this.filter.offset = offset;

    if (fetch) {
      this.fetch(options).then(() => {}).catch(() => {});
    }
    return this;
  }

  /**
   * Set the skip of the listing request.
   * @param skip Request skips.
   * @param fetch Define to auto fetch the data or not.
   * @param options Request options, used when the fetch is true.
   */
  public skip(skip: number, fetch?: boolean, options?: RequestOptions<T>): this {
    this.filter.skip = skip;

    if (fetch) {
      this.fetch(options).then(() => {}).catch(() => {});
    }
    return this;
  }

  /**
   * Go to a specific page of the request.
   * @param page Request page.
   * @param fetch Define to auto fetch the data or not.
   * @param options Request options, used when the fetch is true.
   */
  public goto(page: number, fetch?: boolean, options?: RequestOptions<T>): this {
    this.meta.page = page;
    this.filter.offset = this.filter.limit * (this.meta.page - 1);

    if (fetch) {
      this.fetch(options).then(() => {}).catch(() => {});
    }
    return this;
  }

  /**
   * Set the reponse order.
   * @param orders Array field name order, with ASC or DESC option, or [[OrderFilter]] object.
   *
   * @example
   * ```typescript
   * query.order('firstName ASC', 'createdAt DESC');
   * ```
   */
  public order(orders: OrderFilter<T>): this;
  public order(...orders: string[]): this;
  public order(...orders: Array<string | OrderFilter<T>>): this {
    if (typeof orders[0] === 'object') {
      this.filter.order = orders[0] as OrderFilter<T>;
    } else {
      if (!this.filter.order) {
        this.filter.order = {};
      }

      for (const order of orders as Array<string>) {
        const [key, direction = 'ASC'] = order.split(' ');
        this.filter.order[key] = direction;
      }
    }

    return this;
  }

  /**
   * Set the where filter of the request.
   * @param where Where filter.
   * @param fetch Define to auto fetch the data or not.
   */
  public where(where: WhereFilter<T> | WhereFilter<T>[], fetch?: boolean, options?: RequestOptions<T>): this {
    this.filter.where = where;

    if (fetch) {
      this.fetch(options).then(() => {}).catch(() => {});
    }
    return this;
  }

  /**
   * Set the include filter of the request.
   * @param include Relation to be included.
   * @param fetch Define to auto fetch the data or not.
   * @param options Request options, used when the fetch is true.
   */
  public include(include: InclusionFilter | InclusionFilter[], fetch?: boolean, options?: RequestOptions<T>): this {
    this.filter.include = include;

    if (fetch) {
      this.fetch(options).then(() => {}).catch(() => {});
    }
    return this;
  }

  /**
   * Set the filter to include/exclude fields from the data.
   * @param fields Array field names for inludes, or object {[key]:boolean} to include or exlcude.
   */
  public fields(fields: FieldList<T> | Fields<T>): this {
    if (Array.isArray(fields)) {
      if (!this.filter.fields) {
        this.filter.fields = {};
      }

      for (const key of fields) {
        this.filter.fields[key] = true;
      }
    } else {
      this.filter.fields = fields;
    }
    return this;
  }

  /**
   * Set the query parameter.
   * @param key Parameter name.
   * @param value Parameter value.
   * @param fetch Define to auto fetch the data or not.
   * @param options Request options, used when the fetch is true.
   */
  public set(key: string, value: any, fetch?: boolean, options?: RequestOptions<T>): this {
    this.params[key] = value;

    if (fetch) {
      this.fetch(options).then(() => {}).catch(() => {});
    }
    return this;
  }

  /**
   * Remove the query parameter.
   * @param key Parameter name.
   * @param fetch Define to auto fetch the data or not.
   * @param options Request options, used when the fetch is true.
   */
  public rem(key: string, fetch?: boolean, options?: RequestOptions<T>): this {
    delete this.params[key];

    if (fetch) {
      this.fetch(options).then(() => {}).catch(() => {});
    }
    return this;
  }

  /**
   * Prepend path to the endpoint.
   * @param path Path to be prepended.
   */
  public prefix(path: string): this {
    this.options.prefix = path;
    return this;
  }

  /**
   * Append path to the endpoint.
   * @param path Path to be appended.
   */
  public suffix(path: string): this {
    this.options.suffix = path;
    return this;
  }

  /**
   * Toggle entity status to be selected or nort.
   * @param entity
   */
  public toggleSelect(entity: Entity<T>): void {
    entity.selected = !entity.selected;
  }

  /**
   * Toggle all entities status to be selected or not.
   */
  public toggleSelectAll(): void {
    if (this.allEntitiesSelected) {
      this.entities.forEach(entity => entity.selected = false);
    } else {
      this.entities.forEach(entity => entity.selected = true);
    }
  }

  /**
   * Subscribe to the Repository events.
   * @param handler Function to call when an event emitted.
   */
  public async subscribe(handler?: EventHandler<T>): Promise<Unsubscriber | void> {
    const { repository } = this;
    const { datasource } = repository;
    if (!this.subscription) {
      const filter = JSON.parse(JSON.stringify(this.filter));
      if (filter.order) filter.order = transformOrders(filter.order);

      const optn = JSON.parse(JSON.stringify({ filter }));
      this.subscription = await repository.subscribe(async (event) => {
        this.events.emit({ type: 'changes', data: event });

        const { data } = event;
        if (data.id) {
          for (const entity of this.entities) {
            if (data.id === entity.id) {
              if (event.type === 'delete') {
                entity.data.deletedAt = new Date();
              } else if (event.type === 'put' || event.type === 'patch') {
                for (const [key, value] of Object.entries(data)) {
                  entity.data[key] = value;
                }
              }
            }
          }
        }

        await this.refresh();
      }, optn);
    }

    if (handler) {
      return this.events.subscribe(handler);
    }
  }

  /**
   * Unsubscribe the real time event to prevent overload the performance.
   */
  public async unsubscribe(): Promise<void> {
    if (this.subscription) {
      await this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  /**
   * Count the data of the current query.
   * @param options Request options.
   */
  public async count(options: RequestOptions<T> = {}): Promise<this> {
    if (this.filter.where) options.params = { where: this.filter.where };
    this.meta.total = await this.repository.count(options);
    this.meta.totalPages = Math.ceil((this.meta.total / this.filter.limit));
    return this;
  }

  /**
   * Go to the next page.
   * @param options Request options.
   */
  public async next(options?: RequestOptions<T>): Promise<Entity<T>[]> {
    this.goto(this.meta.page + 1, false, options);
    return await this.fetch(options);
  }

  /**
   * Go to the previous page.
   * @param options Request options.
   */
  public async prev(options?: RequestOptions<T>): Promise<Entity<T>[]> {
    this.goto(this.meta.page - 1, false, options);
    return await this.fetch(options);
  }

  /**
   * Fetch the remote data.
   * @param options Request options.
   */
  public async fetch(options: RequestOptions<T> = {}): Promise<Entities<T>> {
    this.state = 'loading';

    const filter = JSON.parse(JSON.stringify(this.filter));
    if (filter.order) filter.order = transformOrders(filter.order);

    try {
      const optn = JSON.parse(JSON.stringify({
        ...this.options, ...options, filter, params: this.params
      }));
      const data = await this.repository.find(optn);

      this.entities = data;
      this.state = 'complete';

      if (this.subscription && !options.keepSubscription) {
        await this.unsubscribe();
        await this.subscribe();
      }

      return data;
    } catch (error) {
      throw error;
      this.state = 'complete';
    }
  }

  /**
   * Load more data from the API and append the result to the existing entities.
   * @param options Request options.
   */
  public async more(options: RequestOptions<T> = {}): Promise<Entities<T>> {
    this.state = 'load-more';

    const filter = JSON.parse(JSON.stringify(this.filter));
    if (filter.order) filter.order = transformOrders(filter.order);

    try {
      const offset = (this.filter.offset || 0) + this.filter.limit;
      const optn = JSON.parse(JSON.stringify({
        ...this.options, ...options,
        filter: { ...filter, offset },
        params: this.params
      }));
      const data = await this.repository.find(optn);

      if (data.length) {
        this.offset(offset);
        this.entities.push(...data);
      }

      this.state = 'complete';
      return data;
    } catch (error) {
      throw error;
      this.state = 'complete';
    }
  }

  /**
   * Update the current query data by re-fetching the data from the API.
   */
  public async refresh(): Promise<void> {
    await this.fetch({
      keepSubscription: true,
      force: true
    });
  }

  /**
   * Perform a PATCH /{ENDPOINT}/{ID} request.
   * @param id Entity ID.
   * @param body Request body.
   * @param options Request options.
   */
  public async update(id: string, body: EntityPart<T>, options: RequestOptions<T> = {}): Promise<void> {
    return await this.repository.update(id, body, options);
  }

  /**
   * Update all the entities at once, with the same data.
   * @param body Request body.
   * @param options Request options.
   */
  public async updateAll(body: EntityPart<T>, options: RequestOptions<T> = {}): Promise<void> {
    const entities = options.selectedOnly ? this.selectedEntities : this.entities;
    return await this.repository.updateAll(entities, body, options);
  }

  /**
   * Perform a PUT /{ENDPOINT}/{ID} request.
   * @param id Entity ID.
   * @param body Request body.
   * @param options Request options.
   */
  public async replace(id: string, body: T, options: RequestOptions<T> = {}): Promise<void> {
    return await this.repository.replace(id, body, options);
  }

  /**
   * Perform a DELETE /{ENDPOINT}/{ID} request.
   * @param id Entity ID.
   * @param options Request options.
   */
  public async delete(id: string, options: RequestOptions<T> = {}): Promise<void> {
    return await this.repository.delete(id, options);
  }

  /**
   * Delete all the entities at once.
   * @param options Request options.
   */
  public async deleteAll(options: RequestOptions<T> = {}): Promise<void> {
    const entities = options.selectedOnly ? this.selectedEntities : this.entities;
    return await this.repository.deleteAll(entities, options);
  }
}

/**
 * Transform [[OrderFilter]] object into array `KEY DIRECTION` format.
 * @param orders
 */
function transformOrders(orders: OrderFilter<any>) {
  return Object.keys(orders).map(key => `${key} ${orders[key]}`);
}
