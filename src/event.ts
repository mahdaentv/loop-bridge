import { RequestEvent } from './interfaces';

export type EventHandler<T> = (event: RequestEvent<T>) => void;
export type Unsubscriber = () => void;

export class EventEmitter<T> {
  listeners: EventHandler<T>[] = [];

  emit(event: RequestEvent<T>): void {
    for (const handler of this.listeners) {
      if (typeof handler === 'function') {
        handler(event);
      }
    }
  }

  subscribe(handler: EventHandler<T>): Unsubscriber {
    if (Array.isArray(this.listeners)) {
      if (typeof handler === 'function' && !this.listeners.includes(handler)) {
        this.listeners.push(handler);
        return () => this.unsubscribe(handler);
      }
    }
  }

  unsubscribe(handler: EventHandler<T>): void {
    if (Array.isArray(this.listeners)) {
      this.listeners.splice(this.listeners.indexOf(handler), 1);
    }
  }
}
