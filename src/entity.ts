import {
  EntityData,
  EntityPart,
  QueryMetadata,
  RepositoryRelations,
  RequestFilter,
  RequestOptions
} from './interfaces';
import { Repository } from './repository';
import { diff } from './util';
import { EventEmitter, EventHandler, Unsubscriber } from './event';
import { SchemaErrorContext } from './schema';
import { Query } from './query';
import { Subscription } from 'event-bridge/dist/client';

/**
 * A class that wrap the data from the API with a predefined methods to simplify managing data.
 * This is the defaualt Entity that will be used to create new Entity by the Repository.
 */
export class Entity<T extends EntityData> {
  public readyState: 'loading' | 'complete' = 'complete';
  /** Original data to compare the changes between the local data. **/
  private remote: T;
  private subscription: Subscription<T>;
  /** Event emitter that will emit an event when there are some changes. **/
  public events: EventEmitter<T> = new EventEmitter();
  /** Schema error context to show which fields that need to be fixed. **/
  public errors: null | SchemaErrorContext;
  /** A free property to mark the entity as selected. **/
  public selected?: boolean;

  /** Predefined getter to return the Entity ID. **/
  get id(): string {
    return this.data.id;
  }

  /** Predefined getter to return the Entity's created at. **/
  get createdAt(): Date {
    return this.data.createdAt;
  }

  /** Predefined getter to return the Entity's updated at. **/
  get updatedAt(): Date {
    return this.data.updatedAt;
  }

  /** Predefined getter to return the Entity's deleted at. **/
  get deletedAt(): Date {
    return this.data.deletedAt;
  }

  /** Get the changed properties. **/
  public get changes(): any {
    return diff(this.toJSON(), this.remote);
  }

  /** Tell that the Entity has changes or not. **/
  public get hasChanges(): boolean {
    return Object.keys(this.changes).length > 0;
  }

  /** Check does the entity data is valid or not. Useful when the repository using [[Schema]]. **/
  public get valid(): boolean {
    try {
      this.repository.ensureSchemas(this.data);
      this.errors = null;
      return true;
    } catch (error) {
      this.errors = error.context;
      return false;
    }
  }

  /**
   * Create Entity instance.
   * @param repository Repository to manage the data.
   * @param data Data object to be managed.
   * @param options Request options to be included when calling the entity methods if no `options` param given.
   */
  constructor(protected repository: Repository<T>, public data: T, private options?: RequestOptions<T>) {
    this.remote = JSON.parse(this.toJSON(true));
    this.transformRelations();
  }

  /**
   * Update the entity data without doing any request to the API.
   * @param data Partial entity data.
   */
  public assign(data: EntityPart<T>): this {
    Object.assign(this.data, data);
    this.transformRelations();
    this.remote = JSON.parse(this.toJSON(true));
    return this;
  }

  /**
   * Save the changes made to the Entity data.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * user.data.firstName = 'Johnny';
   * await user.save().then(console.log);
   * ```
   */
  public async save(options?: RequestOptions<T>): Promise<void> {
    this.readyState = 'loading';
    if (this.id) {
      await this.repository.update(this.data.id, this.changes, options || this.options);
      this.remote = JSON.parse(this.toJSON(true));
    } else {
      const remote = await this.repository.create(this.changes, options || this.options);
      this.data = remote.data;
      this.transformRelations();
      this.remote = JSON.parse(remote.toJSON(true));
    }

    this.transformRelations();
    this.events.emit({ type: 'update', data: this.data });
    this.readyState = 'complete';
  }

  /**
   * Perform a `PATCH /{ENDPOINT}/{ID}` request.
   * @param body Request body.
   * @param options Requeest options.
   *
   * @example
   * ```typescript
   * user.update({ firstName: 'Johnny' }).then(console.log);
   * ```
   */
  public async update(body: EntityPart<T> = {}, options?: RequestOptions<T>): Promise<void> {
    this.readyState = 'loading';
    await this.repository.update(this.data.id, { ...this.changes, ...body }, options || this.options);

    Object.assign(this.data, body);
    this.transformRelations();
    this.remote = JSON.parse(this.toJSON(true));
    this.events.emit({ type: 'update', data: this.data });
    this.readyState = 'complete';
  }

  /**
   * Perform a `DELETE /{ENDPOINT}/{ID}` request.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * user.delete().then(console.log);
   * ```
   */
  public async delete(options?: RequestOptions<T>): Promise<void> {
    this.readyState = 'loading';
    await this.repository.delete(this.data.id, options);
    this.data.deletedAt = new Date();
    this.transformRelations();
    this.events.emit({ type: 'delete', data: this.data });
    this.readyState = 'complete';
  }

  /**
   * Update the local data with the remote data.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * user.refresh().then(console.log);
   * ```
   */
  public async refresh(options?: RequestOptions<T>): Promise<void> {
    this.readyState = 'loading';
    const updated = await this.repository.findOne(this.id, {
      ...(options || this.options), force: true
    });
    this.data = updated.data;
    this.transformRelations();
    this.remote = JSON.parse(updated.toJSON(true));
    this.events.emit({ type: 'changes', data: this.data });
    this.readyState = 'complete';
  }

  /**
   * Transform the Entity data to JSON object or JSON string.
   * @param stringify Define to stringify the JSON or not.
   * @param self Exclude relation fields from the JSON.
   *
   * @example
   * ```typescript
   * user.toJSON(); // { firstName, lastName }
   * user.toJSON(true); // { "firstName", "lastName" }
   * ```
   */
  public toJSON(stringify?: true, self?: boolean): string;
  public toJSON(stringify?: undefined | false, self?: boolean): object;
  public toJSON(stringify?: boolean, self: boolean = true): object | string {
    const json = {};

    for (const [key, value] of Object.entries(this.data)) {
      if (value instanceof Entity) {
        if (!self) {
          json[key] = value.toJSON();
        }
      } else if (value instanceof Entities) {
        if (!self) {
          json[key] = value.map(item => item.toJSON());
        }
      } else if (value instanceof Date) {
        json[key] = value.toISOString();
      } else {
        json[key] = value;
      }
    }

    return stringify ? JSON.stringify(json) : JSON.parse(JSON.stringify(json));
  }

  /**
   * Subscribe for real time event when there is `POST`, `PUT`, `PATCH`, or `DELETE` event
   * on the server, to update the local data.
   * @param handler Function to handle the event.
   */
  public async subscribe(handler?: EventHandler<T>): Promise<Unsubscriber | void> {
    if (!this.subscription) {
      this.subscription = await this.repository
        .subscribe(this.data.id, async (event) => {
          if (event.data.id === this.id) {
            if (event.type === 'delete') {
              this.data.deletedAt = new Date();
              this.events.emit({ type: 'delete', data: this.data });
            } else if (['put', 'patch'].includes(event.type)) {
              this.events.emit({ type: 'update', data: this.data });
              for (const [key, value] of Object.entries(event.data)) {
                this.data[key] = value;
              }

              await this.refresh();
            }
          } else {
            this.events.emit({ type: 'changes', data: this.data });
            await this.refresh();
          }
        });
    }
    if (handler) {
      return this.events.subscribe(handler);
    }
  }

  /**
   * Unsubscribe the real time event, so don't overload the performance.
   */
  public async unsubscribe(): Promise<void> {
    if (this.subscription) {
      await this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  /**
   * Internal method to transform the `hasOne` and `hasMany` relations.
   */
  public transformRelations(force?: boolean) {
    if (this.repository.datasource.configs.transformRelations || force) {
      this.transformBelongsTo();
      this.transformHasOne();
      this.transformHasMany();
    }
  }

  /**
   * Internal method to transform the `belongsTo` relation. This method will find if the data contains
   * the defined relation `localField` and transform the data to be a new Entity.
   */
  public transformBelongsTo() {
    const { belongsTo } = (this.repository.configs.relations || {}) as RepositoryRelations;

    if (belongsTo) {
      for (const rel of belongsTo) {
        const child = this.data[rel.localField];
        if (child && !(child instanceof Entity)) {
          const instance = this.repository.datasource.getRepository(rel.repository);
          if (instance) {
            this.data[rel.localField] = instance.createEntity(child);
          }
        }
      }
    }
  }

  /**
   * Internal method to transform the `hasOne` relation. This method will find if the data contains
   * the defined relation localField, and transform the data to be a new Entity.
   */
  public transformHasOne() {
    const { relations } = this.repository.configs;

    if (relations && relations.hasOne) {
      for (const rel of relations.hasOne) {
        const child = this.data[rel.localField];

        if (child && !(child instanceof Entity)) {
          const instance = this.repository.datasource.getRepository(rel.repository);

          if (instance) {
            this.data[rel.localField] = instance.createEntity(child);
          }
        }
      }
    }
  }

  /**
   * Internal method to transform the `hasMany` relation. This method will find if the data contains
   * the defined relation key, and tranform the data to be a new Entities.
   */
  public transformHasMany() {
    const { relations } = this.repository.configs;

    if (relations && relations.hasOne) {
      for (const rel of relations.hasOne) {
        const childs = this.data[rel.localField];

        if (childs && !(childs instanceof Entities)) {
          const instance = this.repository.datasource.getRepository(rel.repository);

          if (instance) {
            this.data[rel.localField] = instance.createEntity(childs);
            this.data[rel.localField] = new EntityRelation(instance, rel.foreignKey, this.id);
          }
        }
      }
    }
  }
}

/**
 * A class constructor that extends Entity.
 */
export type EntityConstructor<T extends EntityData> = { new(...args: any[]): Entity<T> };

/**
 * An extended Array that contains predefined methods to manage the data.
 */
export class Entities<T> extends Array<Entity<T>> {
  public meta: QueryMetadata;

  /** Get the selected entities. **/
  public get selected(): Entity<T>[] {
    return this.filter(entity => entity.selected);
  }

  /** Tell does some entities are selected. **/
  public get fewSelected(): boolean {
    const selected = this.selected.length;
    return selected > 0 && selected < this.length;
  }

  /** Tell does all entities are selected. **/
  public get allSelected(): boolean {
    return this.selected.length === this.length;
  }

  /**
   * Create an Entities instance.
   * @param repository Repository instance.
   * @param entities Entity list.
   */
  constructor(private repository: Repository<T>, ...entities: Entity<T>[]) {
    super(...entities);
  }

  /**
   * Update entity data of the all entities without doing a request.
   * @param data Partial entity data.
   */
  public assign(data: EntityPart<T>): void {
    this.forEach(entity => entity.assign(data));
  }

  /**
   * Perform a PATCH /{ENDPOINT}/{ID} request.
   * @param id Entity ID.
   * @param body Request body.
   * @param options Request options.
   */
  public async update(id: string, body: EntityPart<T>, options: RequestOptions<T> = {}): Promise<void> {
    return await this.repository.update(id, body, options);
  }

  /**
   * Update all the entities at once, with the same data.
   * @param body Request body.
   * @param options Request options.
   */
  public async updateAll(body: EntityPart<T>, options: RequestOptions<T> = {}): Promise<void> {
    const entities = options.selectedOnly ? this.selected : this;
    return await this.repository.updateAll(entities, body, options);
  }

  /**
   * Perform a PUT /{ENDPOINT}/{ID} request.
   * @param id Entity ID.
   * @param body Request body.
   * @param options Request options.
   */
  public async replace(id: string, body: T, options: RequestOptions<T> = {}): Promise<void> {
    return await this.repository.replace(id, body, options);
  }

  /**
   * Perform a DELETE /{ENDPOINT}/{ID} request.
   * @param id Entity ID.
   * @param options Request options.
   */
  public async delete(id: string, options: RequestOptions<T> = {}): Promise<void> {
    return await this.repository.delete(id, options);
  }

  /**
   * Delete all the entities at once.
   * @param options Request options.
   */
  public async deleteAll(options: RequestOptions<T> = {}): Promise<void> {
    const entities = options.selectedOnly ? this.selected : this;
    return await this.repository.deleteAll(entities, options);
  }

  /**
   * Transform all the entities to be an array of JSON object or JSON string.
   * @param stringify Define to stringify the JSON or not.
   */
  public toJSON(stringify?: true): string;
  public toJSON(stringify?: undefined | false): object[];
  public toJSON(stringify?: boolean): object[] | string {
    const json = [];

    for (const item of this) {
      if (item instanceof Entity) {
        json.push(item.toJSON());
      } else {
        json.push(item);
      }
    }

    return stringify ? JSON.stringify(json) : json;
  }
}

/**
 * Helper that perform relationships operations such find Projects of User.
 */
export class EntityRelation<T> {
  /**
   * Create new EntityRelation instance.
   * @param repository Related repository.
   * @param foreignKey Foregin key to find the related entity id.
   * @param foreignId ID to be appended to the related repository endpoint.
   */
  constructor(private repository: Repository<T>,
              private foreignKey: string,
              private foreignId: string) {}

  /**
   * Method to query related repository's entities.
   * @param name Query name for a named query.
   * @param filter Request filter.
   * @param options Request options.
   */
  public query(filter?: RequestFilter<T>, options?: RequestOptions<T>): Query<T>;
  public query(name?: string, filter?: RequestFilter<T>, options?: RequestOptions<T>): Query<T>;
  public query(...args: any[]): Query<T> {
    const query = this.repository.query(...args);
    query.set(this.foreignKey, this.foreignId);
    return query;
  }

  /**
   * Method to get all related repository's entitis.
   * @param options Request options.
   */
  public async find(options: RequestOptions<T> = {}): Promise<Entities<T>> {
    if (!options.params) {
      options.params = {};
    }
    options.params[this.foreignKey] = this.foreignId;
    return await this.repository.find(options);
  }

  /**
   * Method to create a related repository's entity.
   * @param body Request body.
   * @param options Request options.
   */
  public async create(body: T, options: RequestOptions<T> = {}): Promise<Entity<T>> {
    return await this.repository.create({
      ...body,
      [this.foreignKey]: this.foreignId
    }, options);
  }
}
