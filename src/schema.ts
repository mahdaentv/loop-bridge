/**
 * A list of schema definitions.
 */
export interface SchemaDefinitions {
  [key: string]: SchemaDefinition;
}

/**
 * Schema definition to tell the validator how to process the data.
 */
export interface SchemaDefinition {
  /** Expected value type of the property. **/
  type: 'string' | 'date' | 'number' | 'object' | 'array' | 'boolean';
  /** Tell the validator that the property is required and must be defined. **/
  required?: boolean;
  /** Tell the validator that the required property must be defined from the remote data, but ignored for the outgoing data. **/
  generated?: boolean;
  /** Value to be set to the property if not defined by user. **/
  default?: any | valueMaker;
}

/**
 * Function to create a default value, and giving a data as a reference when calling the function.
 */
export type valueMaker = (data: any) => any;

/**
 * Error class to give more information about the error when validating Schema.
 */
export class SchemaError extends Error {
  /**
   * Create new error instance.
   * @param context Error context of the schema error.
   */
  constructor(public context: SchemaErrorContext) {
    super(`Schema Error: ${JSON.stringify(context)}`);
  }
}

/**
 * Error context as the information detail of the errors.
 */
export interface SchemaErrorContext {
  [key: string]: {
    expected: string,
    given: any;
  }
}
