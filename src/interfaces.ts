import { AxiosError, AxiosResponse } from 'axios';
import { RepositoryConstructor } from './repository';
import { Entities, Entity } from './entity';
import { SchemaDefinitions } from './schema';
import { LogLevel } from 'event-bridge/dist/logger';

/**
 * Required configuration to create new DataSource instance.
 */
export type DataSourceConfigs = {
  /** DataSource name, required to register it so we can use the decorator. **/
  name: string;
  /** URL of the API. E.g: `http://localhost:3000` **/
  baseURL: string;
  /** Client ID for the WebSocket client, usefull for reconnect. **/
  clientId?: string;
  /** Auto connect WebSocket client. **/
  autoconnect?: boolean;
  /** Always use WebSocket if available. **/
  socketFirst?: boolean;
  /** Set the default headers when creating a new DataSource instance. **/
  headers?: { [key: string]: any };
  /** Set as the default DataSource to get the repositories from. **/
  default?: boolean;
  /** Automatically convert string that like the given types. For date, the date string must be in ISO format. **/
  transformTypes?: Array<'date' | 'number' | 'boolean' | 'null'>;
  /** Automatically transform relations included in the data into Entity/Entities. **/
  transformRelations?: boolean;
  /** Cache the requests on the memory. **/
  cacheRequests?: boolean;
  /** Define the cache expiration time in ms. If the cache is expired, it'll update the cache from the API. **/
  cacheAge?: number;
  /** Set the log level, default to LogLevel.DEBUG. **/
  logLevel?: LogLevel;
}

/**
 * Named parameter to perform a request using the DataSource's methods.
 */
export type Request = {
  /** Request method. **/
  method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
  /** Request path. **/
  path: string;
  /** Request options **/
  options: RequestOptions<any>;
  /** Request body, used when performing `POST, PUT, and PATCH` request. **/
  body?: any;
  protocol?: 'http' | 'ws';
}

/**
 * Request object that will be used to perform the request by the DataSource.
 */
export type RequestObject = {
  /** Transformed request path, including the query params. **/
  path: string;
  /** Transformed request headers. **/
  headers: object;
}

/**
 * Object contains information about the cached requests.
 */
export type RequestCache = {
  response: AxiosResponse;
  createdAt: Date;
  cacheAge?: number;
}

/**
 * Hook data that will be given when invoking the `.beforeRequest()` method.
 */
export type BeforeRequestHook = {
  /** Request method. **/
  method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
  /** Request path. **/
  path: string;
  /** Request body, for `POST, PUT, and PATCH` request. **/
  body?: any;
  /** Request options. **/
  options: RequestOptions<any>;
}

/**
 * Hook data that will be given when invoking the `.afterRequest()` method.
 */
export type AfterRequestHook = {
  /** Request method. **/
  method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
  /** Request object **/
  request?: RequestObject;
  /** Request response **/
  response?: AxiosResponse;
  /** Error response **/
  error?: AxiosError;
}

/**
 * Options to manage the requests.
 */
export type RequestOptions<T> = {
  /** Request params, converted to query params. **/
  params?: RequestParams;
  /** Request filter, converted to `?filter={}`. **/
  filter?: RequestFilter<T>;
  /** String to prepend a path to the request path. **/
  prefix?: string;
  /** String to append a path to the request path. **/
  suffix?: string;
  /** Request headers to merge with the default and custom headers. **/
  headers?: object;
  /** Define to fetch the relation data or not. **/
  withChildren?: boolean;
  /** Define to fetch the relation data recursively or not. **/
  recursive?: boolean;
  /**
   * Callback that will be called after the request success, before the DataSource transform the data.
   * @param response Request response.
   */
  onSuccess?: (response: AxiosResponse) => void,
  /**
   * Callback that will be called when the request errored.
   * @param error Error response.
   */
  onError?: (error: AxiosError) => void;
  /** Set the expiration time of the request. **/
  cacheAge?: number;
  /** For to request to the API if caching enabled. **/
  force?: boolean;
  /** Tell the handler to do bulk actions only for the selected entities. **/
  selectedOnly?: boolean;
  /** Exclude specific fields from the request body. **/
  excludeFields?: string[];
  /** Set the protocol to forward the request. **/
  protocol?: 'http' | 'ws';
  /** Keep the current real time subscription on the query. By default, fetch will re-subscribe to apply the new filters. **/
  keepSubscription?: boolean;
}

/**
 * Event data that will be emitted when the Repository finished performing requests.
 */
export type RequestEvent<T> = {
  /** Request method. **/
  type: 'find' | 'find-one' | 'create' | 'update' | 'update-all' | 'replace' | 'delete' | 'delete-all' | 'changes';
  /** Request options. **/
  options?: RequestOptions<T>;
  /** Request response. **/
  response?: AxiosResponse;
  /** Entity ID for `find, find-one, update, and delete` request. **/
  id?: string;
  /** Request body for `find-one, create, update, and delete` request. **/
  body?: T;
  /** Entity for `find-one` request. **/
  entity?: Entity<T>;
  /** Entities for `find` request. **/
  entities?: Entities<T>;
  /** Event data for reference. **/
  data?: any;
}

/**
 * Request parameters that will be converted to a query params, also to define the relationship keys.
 */
export type RequestParams = {
  [key: string]: any;
}

/**
 * Object to filter the listing Query.
 */
export type RequestFilter<T> = {
  /** Include/exlcude fields in the response. **/
  fields?: Fields<T>;
  /** Request limit. **/
  limit?: number;
  /** Set the orderBy filter. **/
  order?: OrderFilter<T>;
  /** Request offset. **/
  offset?: number;
  /** Request skip. **/
  skip?: number;
  /** Where filter. **/
  where?: WhereFilter<T> | WhereFilter<T>[];
  /** Include filter. **/
  include?: InclusionFilter | InclusionFilter[];
}

/**
 * Include/exclude fields from the response data.
 */
export type Fields<T> = {
  [K in keyof T]?: boolean | Fields<T[K]>;
}
/**
 * List of field names to be included from the response data.
 */
export type FieldList<T> = Array<keyof T>;

export type OrderFilter<T> = { [K in keyof T]?: OrderDirection };
export type OrderDirection = 'ASC' | 'DESC';
export type Operators =
  | 'eq' // Equal
  | 'neq' // Not Equal
  | 'gt' // >
  | 'gte' // >=
  | 'lt' // <
  | 'lte' // <=
  | 'inq' // IN
  | 'nin' // NOT IN
  | 'between' // BETWEEN [val1, val2]
  | 'exists'
  | 'and' // AND
  | 'or' // OR
  | 'like' // LIKE
  | 'nlike' // NOT LIKE
  | 'ilike' // ILIKE'
  | 'nilike' // NOT ILIKE
  | 'regexp'; // REGEXP'

/**
 * Object to filter the listing Query by conditions. Using value such `string` will mark the filter
 * as `equal to`.
 */
export type WhereFilter<T> = {
  [K in keyof T]?: WhereSimpleCondition | WhereConditionFilter<T>;
};
export type WhereConditionFilter<T> = { [K in KeyOf<T>]?: WherePredicateCondition<T[K]> | (T[K] & WhereSimpleCondition) };
export type WhereSimpleCondition = string | number | boolean | Date;
export type WherePredicateCondition<T> = {
  gt?: T;
  gte?: T;
  lt?: T;
  lte?: T;
  eq?: T;
  neq?: T;
  inq?: T[];
  nin?: T[];
  between?: [T, T];
  exists?: boolean;
  like?: T;
  nlike?: T;
  ilike?: T;
  nilike?: T;
  regexp?: string | RegExp;
}
export type KeyOf<T> = Exclude<Extract<keyof T, string>, Operators>;

/**
 * Object to define the included relation. To include multiple relations, use array of keys.
 */
export type InclusionFilter = {
  /** Relation name. **/
  relation: string;
  /** Relation filter. **/
  scope?: RequestFilter<any>;
}

/**
 * Configurations required to create a new Repository instance.
 */
export type RepositoryConfigs = {
  /** Repository name, so we can get the repository instance by name. **/
  name: string;
  /** Repository endpoint, start with `/`. **/
  endpoint: string;
  /** Suffix to be prepended to the final request path. **/
  endpointPrefix?: string;
  /** Suffix to be appended to the final request path. **/
  endpointSuffix?: string;
  /** Repository relations. **/
  relations?: RepositoryRelations;
  /** Default limit of the listing query. **/
  defaultLimit?: number;
  /** Tell the Repository does the API support `/count` path. **/
  hasCounter?: boolean;
  /** Tell the repository does the API has bulk operations support or not. **/
  hasBulkOperations?: boolean;
  /** Add custom headers to all the repository requests. **/
  headers?: { [key: string]: any };
  /** Add schema definitions to the repository. **/
  schemas?: SchemaDefinitions;
  /** Set the expiration time in ms of the repository requests. **/
  cacheAge?: number;
}

export type RelationFields = {
  [localField: string]: RelationRef;
}
export type RelationRef = {
  repository: string | RepositoryConstructor<any> | Function;
  foreignKey: string;
}
export type RelationInclusion<T> = Array<Extract<keyof T, Entity<any> | Entities<any>>>

/**
 * Repository relation definition to define the repository's relationships.
 */
export type RepositoryRelations = {
  /** Tell the repository that it's belongs to another repository. **/
  belongsTo?: RepositoryRelation[];
  /** Tell the repository that the entity will have many entities of another repository. **/
  hasMany?: RepositoryRelation[];
  /** Tell the repository that the entity will have one entity of another repository. **/
  hasOne?: RepositoryRelation[];
}

/**
 * Repository relation definition to manage the relationship.
 */
export type RepositoryRelation = {
  /** Class that extends Repository, or repository name to get the instance from the DataSource. **/
  repository: RepositoryConstructor<any> | string | Function;
  /** Key to find the relation from an entity and query params. **/
  foreignKey: string;
  /** A property name of an entity to find the relationship. **/
  localField: string;
}

/**
 * Repository relation reference.
 */
export type RepositoryRelationRef = {
  key: string;
  value: string;
  endpoint: string;
}

/**
 * Predefined properties of general data returned from an API.
 */
export interface EntityData {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}

/**
 * Payload for update request which only sends some properties.
 */
export type EntityPart<T> = {
  [K in keyof T]?: T[K];
}

/**
 * A pagination metadata of a listing query.
 */
export type QueryMetadata = {
  page: number;
  totalPages: number;
  total: number;
}
