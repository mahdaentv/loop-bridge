import {
  AfterRequestHook,
  BeforeRequestHook,
  DataSourceConfigs,
  Request, RequestCache,
  RequestObject,
  RequestOptions
} from './interfaces';
import { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import axios from 'axios/dist/axios';
import { Repository } from './repository';
import { dataSources } from './decorator';
import { Client, RequestConfig, stringify, Subscription, SubscriptionHandler } from 'event-bridge/dist/client';
import { EventEmitter } from 'event-bridge/dist/event';

const dateRegEx = /^[\d]{4}-[\d]{2}-[\d2]{2}T[\d]{2}:[\d]{2}:[\d]{2}[.\d]?Z$/;
const intRegRex = /^[\d.]+$/;

declare var toString: any;
declare var window: {
  DSCache: {
    [key: string]: string;
  }
}

if (!window.DSCache) {
  window.DSCache = {};
}

/**
 * A REST API Client Library with predefined CRUD methods. DataSource will simplify the requests
 * and responses. DataSource will handle identitcal requests so it wont pefrom multiple requests
 * for the identical requests at the same time.
 */
export class DataSource {
  /** A list of registered repositories. **/
  public repositories: Array<Repository<any>> = [];
  /** Axios instance to forward the requests via HTTP. **/
  public http: AxiosInstance;
  /** EventBridge Client instance to forward the requests via WebSocket. **/
  public wstp: Client;

  public connected: EventEmitter;
  public disconnected: EventEmitter;
  public message: EventEmitter;
  public readyState: number;

  private caches: {
    [key: string]: string
  } = window.DSCache;
  /** Default headers that will be added to the requests. **/
  private defaultHeaders: object = {
    'Accept': '*/*',
    'Content-Type': 'application/json'
  };
  /** A property to save the custom headers to add/replace the default headers. */
  private customHeaders: object = {};

  /** Return a merged headers between default headers and custom headers **/
  private get headers() {
    return { ...this.defaultHeaders, ...this.customHeaders };
  }

  /**
   * Create [[DataSource]] instance so we can use it to perform requests.
   * @param configs A configs that required to create the [[DataSource]] instance.
   */
  constructor(public configs: DataSourceConfigs) {
    this.http = axios.create({ baseURL: configs.baseURL });
    this.wstp = new Client(configs);
    this.connected = this.wstp.connected;
    this.disconnected = this.wstp.disconnected;
    this.message = this.wstp.message;
    this.connected.subscribe(() => this.readyState = this.wstp.readyState);

    if (configs.headers) {
      Object.assign(this.defaultHeaders, configs.headers);
    }

    dataSources.sources[configs.name] = this;
    if (configs.default) {
      dataSources.default = this;
    }
  }

  /**
   * Connect WebSocket client to server.
   * @param reconnect Mark the connection as reconnect, so all queued events will be propagated.
   */
  public connect(reconnect?: boolean) {
    this.wstp.connect(reconnect);
  }

  /**
   * Perform an asynchronus GET request to the API. The response data will be an Object or an Array.
   * @param path Request path.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * datasource.get('/users')
   *   .then(console.log)
   *   .catch(console.error);
   * ```
   */
  public async get(path: string, options: RequestOptions<any> = {}): Promise<AxiosResponse> {
    return await this.request({ method: 'GET', path, options });
  }

  /**
   * Perform an asynchronus POST request to the API. The response data will be an Object.
   * @param path Request path.
   * @param body Request body.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * datasource.post('/users', { firstName: 'Jhonny', lastName: 'Doe' })
   *   .then(console.log)
   *   .catch(console.error);
   * ```
   */
  public async post(path: string, body: any, options: RequestOptions<any> = {}): Promise<AxiosResponse> {
    return await this.request({ method: 'POST', path, options, body });
  }

  /**
   * Perform an asynchronus PATCH request to the API. The response data will be nothing.
   * @param path Request path.
   * @param body Request body.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * datasource.patch('/users/1', { firstName: 'John' })
   *   .then(console.log)
   *   .catch(console.error);
   * ```
   */
  public async patch(path: string, body: any, options: RequestOptions<any> = {}): Promise<AxiosResponse> {
    return await this.request({ method: 'PATCH', path, options, body });
  }

  /**
   * Perform an asynchronus PUT request to the API. The reponse data will be nothing.
   * @param path Request path.
   * @param body Request body.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * datasource.put('/users/1', { firstName: 'John', lastName: 'Doe' })
   *   .then(console.log)
   *   .catch(console.error);
   * ```
   */
  public async put(path: string, body: any, options: RequestOptions<any> = {}): Promise<AxiosResponse> {
    return await this.request({ method: 'PUT', path, options, body });
  }

  /**
   * Perform an asynchronus DELETE request to the API. The response data will be nothing.
   * @param path Request path.
   * @param options Request options.
   *
   * @example
   * ```typescript
   * datasource.delete('/users/1')
   *   .then(console.log)
   *   .catch(console.error);
   * ```
   */
  public async delete(path: string, options: RequestOptions<any> = {}): Promise<AxiosResponse> {
    return await this.request({ method: 'DELETE', path, options });
  }

  /**
   * Subscribe to a path so the subscriber will get a real time updates when there is a `POST`,
   * `PUT`, `PATCH`, or `DELETE` event on the related paths.
   * @param path Path to subscribe to.
   * @param handler Function to handle the event.
   * @param options Request options.
   */
  public async subscribe<T>(path: string, handler: SubscriptionHandler<T>, options: RequestOptions<T> = {}): Promise<Subscription<T>> {
    const { headers = {}, prefix, suffix } = options;

    if (prefix) {
      path = `${prefix}/${path}`;
    }
    if (suffix) {
      path = `${path}/${suffix}`;
    }

    path = path.replace(/[\/]+/g, '/');

    return await this.wstp.subscribe(path, (event) => {
      event.data = this.transformData(event.data || {}) as any;
      return handler(event);
    }, {
      ...options,
      headers: { ...this.headers, ...headers }
    });
  }

  /**
   * Register new Repository to the DataSource.
   * @param instance Repository instance to be requestered.
   *
   * @example
   * ```typescript
   * class Users extends Repository<any> {}
   * const users = new Users(datasource, { endpoint: 'users', name: 'user' });
   * datasource.addRepository(users);
   * ```
   */
  public addRepository<T extends Repository<any>>(instance: T): void {
    if (!this.repositories.includes(instance)) {
      this.repositories.push(instance);
    }
  }

  /**
   * Get the instance of Repository from the DataSource. If the repository is not available,
   * it wont cause an error but return nothing.
   * @param type [[Repository]] name or Class that extend [[Repository]].
   *
   * @example
   * ```typescript
   * class Projects extends Repository<any> {}
   * const users = datasource.getRepository('user');
   * const projects = datasource.getRepository(Projects);
   * ```
   */
  public getRepository<T extends Repository<any>>(type: string | any): T {
    if (typeof type === 'string') {
      for (const instance of this.repositories) {
        if (instance.configs.name === type) {
          return instance as T;
        }
      }
    } else {
      for (const instance of this.repositories) {
        if (instance instanceof type) {
          return instance as T;
        }
      }
    }
  }

  /**
   * Add/Update a custom header to be added to the request headers. The header will be added to all requests.
   * @param key Header name. E.g: `Authorization`.
   * @param value Header value. E.g: `Bearer ${token}`
   *
   * @example
   * ```typescript
   * datasource.setHeader('Authorization', `Bearer ${token}`);
   * ```
   *
   */
  public setHeader(key: string, value: any): void {
    this.customHeaders[key] = value;
  }

  /**
   * Remove a custom header.
   * @param key Header name. E.g: `Authorization`.
   *
   * @example
   * ```typescript
   * datasource.remHeader('Authorization');
   * ```
   */
  public remHeader(key: string): void {
    delete this.customHeaders[key];
  }

  /**
   * Clear the cached requests to free up the memory usage.
   */
  public clearCache(): void {
    this.caches = {};
  }

  /**
   * Perform an asynchronus request to the API. The response data will be Object or an Array.
   * This method is protected so only accessible from the instance or sub class instance.
   * @param method Request method name.
   * @param path Request path. E.g: `/users`
   * @param options Request options.
   * @param body Request body, only required for `POST`, `PATCH`, and `PUT` request.
   */
  protected async request({ method, path, options, body }: Request) {
    try {
      const { cacheRequests, cacheAge } = this.configs;
      const cacheKey = JSON.stringify({ method, path, options });

      if (method === 'GET' && cacheRequests && this.caches[cacheKey] && !options.force) {
        const cache = this.caches[cacheKey] ? JSON.parse(this.caches[cacheKey]) : this.caches[cacheKey];

        if (cache.cacheAge || cacheAge) {
          const rdiff = (new Date().getTime() - cache.createdAt.getTime());

          if (cache.cacheAge) {
            if (rdiff < cache.cacheAge) {
              this.transformData(cache.response.data);
              return cache.response;
            }
          } else {
            if (rdiff < cacheAge) {
              this.transformData(cache.response.data);
              return cache.response;
            }
          }
        } else {
          this.transformData(cache.response.data);
          return cache.response;
        }
      }

      if (this.beforeRequest) await this.beforeRequest({ method, path, body, options });
      const request = this.createRequest(path, options);

      try {
        const config: AxiosRequestConfig = {
          url: request.path,
          headers: request.headers,
          method: method.toLowerCase() as any
        };
        if (body) config.data = body;

        const { protocol = this.configs.socketFirst ? 'ws' : 'http' } = options || {};
        let response;
        if (protocol === 'ws' && this.wstp.readyState === WebSocket.OPEN) {
          response = await this.wstp.request(config as RequestConfig<any>);
        } else {
          response = await this.http.request(config);
        }

        if (this.transformResponse) await this.transformResponse(response, request, method);

        if (typeof options.onSuccess === 'function') {
          await options.onSuccess(response);
        }

        this.transformData(response.data);
        if (this.afterRequest) await this.afterRequest({ method, request, response });

        if (method === 'GET' && cacheRequests) {
          const cache: RequestCache = { response, createdAt: new Date() };

          if (options.cacheAge) {
            cache.cacheAge = options.cacheAge;
          }

          this.caches[cacheKey] = JSON.stringify(cache);
        }

        return response;
      } catch (error) {
        if (this.failedRequest) await this.failedRequest({ method, request, error });
        if (typeof options.onError === 'function') {
          await options.onError(error);
        }

        throw error;
      }
    } catch (error) {
      if (this.failedRequest) await this.failedRequest({ method, error });
      if (typeof options.onError === 'function') {
        await options.onError(error);
      }

      throw error;
    }
  }

  /**
   * A request hook that will be invoked before performing a request. Will be useful for a sub class
   * that need to modify the requests data.
   * @param data Data contains request information that will be sent.
   *
   * @example
   * ```typescript
   * class MainSource extends DataSource {
   *   protected async beforeRequest(data: BeforeRequestHook): Promise<void> {
   *     const token = localStorage.getItem('token');;
   *     if (token) {
   *       data.options.headers = { 'X-Auth-Token': token };
   *     }
   *   }
   * }
   * ```
   */
  protected async beforeRequest?(data: BeforeRequestHook): Promise<void>;

  /**
   * Internal method to define the request object such prepending path prefix.
   * @param path Request path.
   * @param params Parameters that will be converted to query params.
   * @param filter Parameters that will be converted to `filter={}` query params.
   * @param headers Request headers.
   * @param prefix A path that will be prepended to the request path.
   * @param suffix A path that will be appended to the request path.
   */
  protected createRequest(path: string, {
    params,
    filter,
    headers,
    prefix,
    suffix
  }: RequestOptions<any> = {}): RequestObject {
    const request: RequestObject = { path, headers: this.headers };
    const queries: string[] = [];

    if (params && Object.keys(params).length) {
      queries.push(stringify(params));
    }

    if (filter) {
      queries.push(`filter=${JSON.stringify(filter)}`);
    }

    if (headers) {
      request.headers = { ...request.headers, ...headers };
    }

    if (suffix) {
      request.path = `${request.path}/${suffix}`;
    }

    if (queries.length) {
      request.path = `${request.path}?${queries.join('&')}`;
    }

    if (prefix) {
      request.path = `${prefix}/${request.path}`;
    }

    request.path = request.path.replace(/[\/]+/g, '/');

    return request;
  }

  /**
   * Internal method to transform the JSON data such converting date string into [[Date]] object.
   * @param data Object or Array to be converted.
   */
  protected transformData(data: object | object[]): object | object[] {
    if (Array.isArray(data)) {
      return data.map(item => this.transformData(item));
    } else if (toString.call(data) === '[object Object]') {
      const { transformTypes } = this.configs;
      const toDate = Array.isArray(transformTypes) && transformTypes.includes('date');
      const toNumber = Array.isArray(transformTypes) && transformTypes.includes('number');
      const toBoolean = Array.isArray(transformTypes) && transformTypes.includes('boolean');
      const toNull = Array.isArray(transformTypes) && transformTypes.includes('null');

      for (const [key, value] of Object.entries(data)) {
        // Transform date string to date object.
        if (toDate && dateRegEx.test(value)) {
          data[key] = new Date(value);
        }

        // Transform null/
        if (toNull && value === 'null') {
          data[key] = null;
        }

        // Transform boolean.
        if (toBoolean && value === 'true') {
          data[key] = true;
        }

        if (toBoolean && value === 'false') {
          data[key] = false;
        }

        // Transform numbers.
        if (toNumber && intRegRex.test(value)) {
          data[key] = parseFloat(value);
        }

        // Recursively transform array and object.
        if (Array.isArray(value) || toString.call(value) === '[object Object]') {
          this.transformData(value);
        }
      }

      return data;
    } else {
      return data;
    }
  }

  /**
   * A request hook that will be invoked when the request failed.
   * @param data Request hook data.
   */
  protected async failedRequest?(data: AfterRequestHook): Promise<void>;

  /**
   * A request hook that will be invoked after the request completely finished. Will be useful for a sub class
   * that will doing a custom handling after performing a request.
   * @param data Data contains information about the finished request.
   *
   * @example
   * ```typescript
   * class MainSource extends DataSource {
   *   protected async afterRequeset(data: AfterRequestHook): Promise<void> {
   *     data.response.headers['X-Total'] = data.response.data.total;
   *     data.response.data = data.response.data.items;
   *   }
   * }
   * ```
   */
  protected async afterRequest?(data: AfterRequestHook): Promise<void>;

  /**
   * A request hook that will be invoked after doing request, to transform the response.
   * @param response Axios response object.
   * @param request Request object.
   * @param method Request method.
   */
  protected async transformResponse?(response: AxiosResponse, request: RequestObject, method: string): Promise<void>;
}

/**
 * A class constructor that extends DataSource.
 */
export type DataSourceConstructor = { new(...args: any[]): DataSource };
