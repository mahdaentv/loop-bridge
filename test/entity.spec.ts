import { DataSource, Entity, EntityData, Repository } from '../src';

interface UserSchema {
  firstName: string;
  lastName?: string;
  fullName?: string;
  email: string;
  password: string;
}

interface User extends EntityData, UserSchema {
  photoURL?: string;
  projects?: any[];
}

class Users extends Repository<User> {}

describe('Entity', () => {
  const datasource = new DataSource({ name: 'main', baseURL: 'http://localhost:3999' });
  const users = new Users(datasource, { name: 'user', endpoint: '/users' });
  let user: Entity<User>;

  test('should return User 1.', async () => {
    user = await users.findOne('1');
    expect(user.id).toBe(1);
    expect(user.data.fullName).toBe('User 1')
  });
});
