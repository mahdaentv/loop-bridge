import { DataSource } from '../src';
import { AxiosResponse } from 'axios';

let datasource;

describe('DataSource', () => {
  test('should create new DataSource', () => {
    datasource = new DataSource({ name: 'main', baseURL: 'http://localhost:3999' });
    expect(datasource instanceof DataSource).toBe(true);
  });

  test('new DataSource should not have any repositories', () => {
    expect(datasource.repositories.length).toBe(0);
  });

  test('should response 200 when requesting to /users', async () => {
    const response: AxiosResponse = await datasource.get('/users');
    expect(response.status).toBe(200);
  });

  test('should have 5 users when requesting to /users', async () => {
    const response: AxiosResponse = await datasource.get('/users');
    expect(response.data.length).toBe(5);
  });

  test('should response 200 when requesting to /users/1', async () => {
    const response: AxiosResponse = await datasource.get('/users/1');
    expect(response.status).toBe(200);
  });
});
