import { DataSource, Entities, Entity, EntityData, Repository } from '../src';

interface UserSchema {
  firstName: string;
  lastName?: string;
  email: string;
  password: string;
}

interface User extends EntityData, UserSchema {
  photoURL?: string;
  projects?: any[];
}

class Users extends Repository<User> {}

describe('Repository', () => {
  const datasource = new DataSource({ name: 'main', baseURL: 'http://localhost:3999' });
  let users: Users, newUser: Entity<User>;

  test('should create Users repository.', () => {
    users = new Users(datasource, { name: 'user', endpoint: '/users' });
    expect(users instanceof Repository).toBe(true);
  });

  test('should be registered to the DataSource.', () => {
    const instance = datasource.getRepository(Users);
    expect(instance instanceof Users).toBe(true);
    expect(instance.configs.name).toBe('user');
  });

  test('should be the only registered repository.', () => {
    expect(datasource.repositories.length).toBe(1);
    expect(datasource.repositories[0]).toBe(users);
  });

  test('should create new user: Test.', async () => {
    newUser = await users.create({ firstName: 'Test', email: 'user@email.com', password: 'test' });

    expect(newUser instanceof Entity).toBe(true);
    expect(newUser.id).toBeDefined();
    expect(newUser.data.firstName).toBe('Test');
  });

  test('should return new user: Test.', async () => {
    const remote = await users.findOne(newUser.id);
    expect(remote.id).toBe(newUser.id);
  });

  test('should update user: Test.', async () => {
    let success;

    try {
      await users.update(newUser.id, { firstName: 'Test Updated' });
      success = true;
    } catch (error) {
      success = false;
    }

    expect(success).toBe(true);
  });

  test('should delete user: Test.', async () => {
    let success;
    try {
      await users.delete(newUser.id);
      success = true;
    } catch (e) {
      success = false;
    }
    expect(success).toBe(true);
  });

  test('should return Entities of 5 users.', async () => {
    const items = await users.find();

    expect(items instanceof Entities).toBe(true);
    expect(items.length).toBe(5);
  });

  test('should return Entity of User 1.', async () => {
    const item = await users.findOne('1');

    expect(item instanceof Entity).toBe(true);
    expect(item.id).toBe(1);
  });
});
