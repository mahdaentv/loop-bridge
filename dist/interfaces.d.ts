import { AxiosError, AxiosResponse } from 'axios';
import { RepositoryConstructor } from './repository';
import { Entities, Entity } from './entity';
import { SchemaDefinitions } from './schema';
import { LogLevel } from 'event-bridge/dist/logger';
export declare type DataSourceConfigs = {
    name: string;
    baseURL: string;
    clientId?: string;
    autoconnect?: boolean;
    socketFirst?: boolean;
    headers?: {
        [key: string]: any;
    };
    default?: boolean;
    transformTypes?: Array<'date' | 'number' | 'boolean' | 'null'>;
    transformRelations?: boolean;
    cacheRequests?: boolean;
    cacheAge?: number;
    logLevel?: LogLevel;
};
export declare type Request = {
    method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
    path: string;
    options: RequestOptions<any>;
    body?: any;
    protocol?: 'http' | 'ws';
};
export declare type RequestObject = {
    path: string;
    headers: object;
};
export declare type RequestCache = {
    response: AxiosResponse;
    createdAt: Date;
    cacheAge?: number;
};
export declare type BeforeRequestHook = {
    method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
    path: string;
    body?: any;
    options: RequestOptions<any>;
};
export declare type AfterRequestHook = {
    method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
    request?: RequestObject;
    response?: AxiosResponse;
    error?: AxiosError;
};
export declare type RequestOptions<T> = {
    params?: RequestParams;
    filter?: RequestFilter<T>;
    prefix?: string;
    suffix?: string;
    headers?: object;
    withChildren?: boolean;
    recursive?: boolean;
    onSuccess?: (response: AxiosResponse) => void;
    onError?: (error: AxiosError) => void;
    cacheAge?: number;
    force?: boolean;
    selectedOnly?: boolean;
    excludeFields?: string[];
    protocol?: 'http' | 'ws';
    keepSubscription?: boolean;
};
export declare type RequestEvent<T> = {
    type: 'find' | 'find-one' | 'create' | 'update' | 'update-all' | 'replace' | 'delete' | 'delete-all' | 'changes';
    options?: RequestOptions<T>;
    response?: AxiosResponse;
    id?: string;
    body?: T;
    entity?: Entity<T>;
    entities?: Entities<T>;
    data?: any;
};
export declare type RequestParams = {
    [key: string]: any;
};
export declare type RequestFilter<T> = {
    fields?: Fields<T>;
    limit?: number;
    order?: OrderFilter<T>;
    offset?: number;
    skip?: number;
    where?: WhereFilter<T> | WhereFilter<T>[];
    include?: InclusionFilter | InclusionFilter[];
};
export declare type Fields<T> = {
    [K in keyof T]?: boolean | Fields<T[K]>;
};
export declare type FieldList<T> = Array<keyof T>;
export declare type OrderFilter<T> = {
    [K in keyof T]?: OrderDirection;
};
export declare type OrderDirection = 'ASC' | 'DESC';
export declare type Operators = 'eq' | 'neq' | 'gt' | 'gte' | 'lt' | 'lte' | 'inq' | 'nin' | 'between' | 'exists' | 'and' | 'or' | 'like' | 'nlike' | 'ilike' | 'nilike' | 'regexp';
export declare type WhereFilter<T> = {
    [K in keyof T]?: WhereSimpleCondition | WhereConditionFilter<T>;
};
export declare type WhereConditionFilter<T> = {
    [K in KeyOf<T>]?: WherePredicateCondition<T[K]> | (T[K] & WhereSimpleCondition);
};
export declare type WhereSimpleCondition = string | number | boolean | Date;
export declare type WherePredicateCondition<T> = {
    gt?: T;
    gte?: T;
    lt?: T;
    lte?: T;
    eq?: T;
    neq?: T;
    inq?: T[];
    nin?: T[];
    between?: [T, T];
    exists?: boolean;
    like?: T;
    nlike?: T;
    ilike?: T;
    nilike?: T;
    regexp?: string | RegExp;
};
export declare type KeyOf<T> = Exclude<Extract<keyof T, string>, Operators>;
export declare type InclusionFilter = {
    relation: string;
    scope?: RequestFilter<any>;
};
export declare type RepositoryConfigs = {
    name: string;
    endpoint: string;
    endpointPrefix?: string;
    endpointSuffix?: string;
    relations?: RepositoryRelations;
    defaultLimit?: number;
    hasCounter?: boolean;
    hasBulkOperations?: boolean;
    headers?: {
        [key: string]: any;
    };
    schemas?: SchemaDefinitions;
    cacheAge?: number;
};
export declare type RelationFields = {
    [localField: string]: RelationRef;
};
export declare type RelationRef = {
    repository: string | RepositoryConstructor<any> | Function;
    foreignKey: string;
};
export declare type RelationInclusion<T> = Array<Extract<keyof T, Entity<any> | Entities<any>>>;
export declare type RepositoryRelations = {
    belongsTo?: RepositoryRelation[];
    hasMany?: RepositoryRelation[];
    hasOne?: RepositoryRelation[];
};
export declare type RepositoryRelation = {
    repository: RepositoryConstructor<any> | string | Function;
    foreignKey: string;
    localField: string;
};
export declare type RepositoryRelationRef = {
    key: string;
    value: string;
    endpoint: string;
};
export interface EntityData {
    id?: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}
export declare type EntityPart<T> = {
    [K in keyof T]?: T[K];
};
export declare type QueryMetadata = {
    page: number;
    totalPages: number;
    total: number;
};
