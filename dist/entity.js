import { diff } from './util';
import { EventEmitter } from './event';
export class Entity {
    constructor(repository, data, options) {
        this.repository = repository;
        this.data = data;
        this.options = options;
        this.readyState = 'complete';
        this.events = new EventEmitter();
        this.remote = JSON.parse(this.toJSON(true));
        this.transformRelations();
    }
    get id() {
        return this.data.id;
    }
    get createdAt() {
        return this.data.createdAt;
    }
    get updatedAt() {
        return this.data.updatedAt;
    }
    get deletedAt() {
        return this.data.deletedAt;
    }
    get changes() {
        return diff(this.toJSON(), this.remote);
    }
    get hasChanges() {
        return Object.keys(this.changes).length > 0;
    }
    get valid() {
        try {
            this.repository.ensureSchemas(this.data);
            this.errors = null;
            return true;
        }
        catch (error) {
            this.errors = error.context;
            return false;
        }
    }
    assign(data) {
        Object.assign(this.data, data);
        this.transformRelations();
        this.remote = JSON.parse(this.toJSON(true));
        return this;
    }
    async save(options) {
        this.readyState = 'loading';
        if (this.id) {
            await this.repository.update(this.data.id, this.changes, options || this.options);
            this.remote = JSON.parse(this.toJSON(true));
        }
        else {
            const remote = await this.repository.create(this.changes, options || this.options);
            this.data = remote.data;
            this.transformRelations();
            this.remote = JSON.parse(remote.toJSON(true));
        }
        this.transformRelations();
        this.events.emit({ type: 'update', data: this.data });
        this.readyState = 'complete';
    }
    async update(body = {}, options) {
        this.readyState = 'loading';
        await this.repository.update(this.data.id, Object.assign(Object.assign({}, this.changes), body), options || this.options);
        Object.assign(this.data, body);
        this.transformRelations();
        this.remote = JSON.parse(this.toJSON(true));
        this.events.emit({ type: 'update', data: this.data });
        this.readyState = 'complete';
    }
    async delete(options) {
        this.readyState = 'loading';
        await this.repository.delete(this.data.id, options);
        this.data.deletedAt = new Date();
        this.transformRelations();
        this.events.emit({ type: 'delete', data: this.data });
        this.readyState = 'complete';
    }
    async refresh(options) {
        this.readyState = 'loading';
        const updated = await this.repository.findOne(this.id, Object.assign(Object.assign({}, (options || this.options)), { force: true }));
        this.data = updated.data;
        this.transformRelations();
        this.remote = JSON.parse(updated.toJSON(true));
        this.events.emit({ type: 'changes', data: this.data });
        this.readyState = 'complete';
    }
    toJSON(stringify, self = true) {
        const json = {};
        for (const [key, value] of Object.entries(this.data)) {
            if (value instanceof Entity) {
                if (!self) {
                    json[key] = value.toJSON();
                }
            }
            else if (value instanceof Entities) {
                if (!self) {
                    json[key] = value.map(item => item.toJSON());
                }
            }
            else if (value instanceof Date) {
                json[key] = value.toISOString();
            }
            else {
                json[key] = value;
            }
        }
        return stringify ? JSON.stringify(json) : JSON.parse(JSON.stringify(json));
    }
    async subscribe(handler) {
        if (!this.subscription) {
            this.subscription = await this.repository
                .subscribe(this.data.id, async (event) => {
                if (event.data.id === this.id) {
                    if (event.type === 'delete') {
                        this.data.deletedAt = new Date();
                        this.events.emit({ type: 'delete', data: this.data });
                    }
                    else if (['put', 'patch'].includes(event.type)) {
                        this.events.emit({ type: 'update', data: this.data });
                        for (const [key, value] of Object.entries(event.data)) {
                            this.data[key] = value;
                        }
                        await this.refresh();
                    }
                }
                else {
                    this.events.emit({ type: 'changes', data: this.data });
                    await this.refresh();
                }
            });
        }
        if (handler) {
            return this.events.subscribe(handler);
        }
    }
    async unsubscribe() {
        if (this.subscription) {
            await this.subscription.unsubscribe();
            this.subscription = null;
        }
    }
    transformRelations(force) {
        if (this.repository.datasource.configs.transformRelations || force) {
            this.transformBelongsTo();
            this.transformHasOne();
            this.transformHasMany();
        }
    }
    transformBelongsTo() {
        const { belongsTo } = (this.repository.configs.relations || {});
        if (belongsTo) {
            for (const rel of belongsTo) {
                const child = this.data[rel.localField];
                if (child && !(child instanceof Entity)) {
                    const instance = this.repository.datasource.getRepository(rel.repository);
                    if (instance) {
                        this.data[rel.localField] = instance.createEntity(child);
                    }
                }
            }
        }
    }
    transformHasOne() {
        const { relations } = this.repository.configs;
        if (relations && relations.hasOne) {
            for (const rel of relations.hasOne) {
                const child = this.data[rel.localField];
                if (child && !(child instanceof Entity)) {
                    const instance = this.repository.datasource.getRepository(rel.repository);
                    if (instance) {
                        this.data[rel.localField] = instance.createEntity(child);
                    }
                }
            }
        }
    }
    transformHasMany() {
        const { relations } = this.repository.configs;
        if (relations && relations.hasOne) {
            for (const rel of relations.hasOne) {
                const childs = this.data[rel.localField];
                if (childs && !(childs instanceof Entities)) {
                    const instance = this.repository.datasource.getRepository(rel.repository);
                    if (instance) {
                        this.data[rel.localField] = instance.createEntity(childs);
                        this.data[rel.localField] = new EntityRelation(instance, rel.foreignKey, this.id);
                    }
                }
            }
        }
    }
}
export class Entities extends Array {
    constructor(repository, ...entities) {
        super(...entities);
        this.repository = repository;
    }
    get selected() {
        return this.filter(entity => entity.selected);
    }
    get fewSelected() {
        const selected = this.selected.length;
        return selected > 0 && selected < this.length;
    }
    get allSelected() {
        return this.selected.length === this.length;
    }
    assign(data) {
        this.forEach(entity => entity.assign(data));
    }
    async update(id, body, options = {}) {
        return await this.repository.update(id, body, options);
    }
    async updateAll(body, options = {}) {
        const entities = options.selectedOnly ? this.selected : this;
        return await this.repository.updateAll(entities, body, options);
    }
    async replace(id, body, options = {}) {
        return await this.repository.replace(id, body, options);
    }
    async delete(id, options = {}) {
        return await this.repository.delete(id, options);
    }
    async deleteAll(options = {}) {
        const entities = options.selectedOnly ? this.selected : this;
        return await this.repository.deleteAll(entities, options);
    }
    toJSON(stringify) {
        const json = [];
        for (const item of this) {
            if (item instanceof Entity) {
                json.push(item.toJSON());
            }
            else {
                json.push(item);
            }
        }
        return stringify ? JSON.stringify(json) : json;
    }
}
export class EntityRelation {
    constructor(repository, foreignKey, foreignId) {
        this.repository = repository;
        this.foreignKey = foreignKey;
        this.foreignId = foreignId;
    }
    query(...args) {
        const query = this.repository.query(...args);
        query.set(this.foreignKey, this.foreignId);
        return query;
    }
    async find(options = {}) {
        if (!options.params) {
            options.params = {};
        }
        options.params[this.foreignKey] = this.foreignId;
        return await this.repository.find(options);
    }
    async create(body, options = {}) {
        return await this.repository.create(Object.assign(Object.assign({}, body), { [this.foreignKey]: this.foreignId }), options);
    }
}
//# sourceMappingURL=entity.js.map