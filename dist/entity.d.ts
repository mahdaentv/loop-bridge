import { EntityData, EntityPart, QueryMetadata, RequestFilter, RequestOptions } from './interfaces';
import { Repository } from './repository';
import { EventEmitter, EventHandler, Unsubscriber } from './event';
import { SchemaErrorContext } from './schema';
import { Query } from './query';
export declare class Entity<T extends EntityData> {
    protected repository: Repository<T>;
    data: T;
    private options?;
    readyState: 'loading' | 'complete';
    private remote;
    private subscription;
    events: EventEmitter<T>;
    errors: null | SchemaErrorContext;
    selected?: boolean;
    get id(): string;
    get createdAt(): Date;
    get updatedAt(): Date;
    get deletedAt(): Date;
    get changes(): any;
    get hasChanges(): boolean;
    get valid(): boolean;
    constructor(repository: Repository<T>, data: T, options?: RequestOptions<T>);
    assign(data: EntityPart<T>): this;
    save(options?: RequestOptions<T>): Promise<void>;
    update(body?: EntityPart<T>, options?: RequestOptions<T>): Promise<void>;
    delete(options?: RequestOptions<T>): Promise<void>;
    refresh(options?: RequestOptions<T>): Promise<void>;
    toJSON(stringify?: true, self?: boolean): string;
    toJSON(stringify?: undefined | false, self?: boolean): object;
    subscribe(handler?: EventHandler<T>): Promise<Unsubscriber | void>;
    unsubscribe(): Promise<void>;
    transformRelations(force?: boolean): void;
    transformBelongsTo(): void;
    transformHasOne(): void;
    transformHasMany(): void;
}
export declare type EntityConstructor<T extends EntityData> = {
    new (...args: any[]): Entity<T>;
};
export declare class Entities<T> extends Array<Entity<T>> {
    private repository;
    meta: QueryMetadata;
    get selected(): Entity<T>[];
    get fewSelected(): boolean;
    get allSelected(): boolean;
    constructor(repository: Repository<T>, ...entities: Entity<T>[]);
    assign(data: EntityPart<T>): void;
    update(id: string, body: EntityPart<T>, options?: RequestOptions<T>): Promise<void>;
    updateAll(body: EntityPart<T>, options?: RequestOptions<T>): Promise<void>;
    replace(id: string, body: T, options?: RequestOptions<T>): Promise<void>;
    delete(id: string, options?: RequestOptions<T>): Promise<void>;
    deleteAll(options?: RequestOptions<T>): Promise<void>;
    toJSON(stringify?: true): string;
    toJSON(stringify?: undefined | false): object[];
}
export declare class EntityRelation<T> {
    private repository;
    private foreignKey;
    private foreignId;
    constructor(repository: Repository<T>, foreignKey: string, foreignId: string);
    query(filter?: RequestFilter<T>, options?: RequestOptions<T>): Query<T>;
    query(name?: string, filter?: RequestFilter<T>, options?: RequestOptions<T>): Query<T>;
    find(options?: RequestOptions<T>): Promise<Entities<T>>;
    create(body: T, options?: RequestOptions<T>): Promise<Entity<T>>;
}
