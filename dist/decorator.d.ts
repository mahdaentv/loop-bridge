import { DataSource, DataSourceConstructor } from './datasource';
import { SchemaDefinitions } from './schema';
import { RepositoryConstructor } from './repository';
import { RequestFilter, RequestOptions } from './interfaces';
export declare const dataSources: {
    default?: DataSource;
    sources: {
        [key: string]: DataSource;
    };
};
export declare function repository(repositoryName: string, dataSourceName?: string): any;
export declare function repository(repositoryName: string, DataSourceClass?: DataSourceConstructor): any;
export declare function repository(RepositoryClass: RepositoryConstructor<any>, dataSourceName?: string): any;
export declare function repository(RepositoryClass: RepositoryConstructor<any>, DataSourceClass?: DataSourceConstructor): any;
export declare function query<T>(name: string, filter?: RequestFilter<T>, options?: RequestOptions<T>): (constructor: any, key: string) => void;
export declare function entity(repositoryName: string, data?: any): any;
export declare function entity(RepositoryClass: RepositoryConstructor<any>, data?: any): any;
export declare function getRepository(repositoryName: string, dataSourceName?: string): any;
export declare function getRepository(repositoryName: string, DataSourceClass?: DataSourceConstructor): any;
export declare function getRepository(RepositoryClass: RepositoryConstructor<any>, dataSourceName?: string): any;
export declare function getRepository(RepositoryClass: RepositoryConstructor<any>, DataSourceClass?: DataSourceConstructor): any;
export declare function Schema(schemas: SchemaDefinitions): (constructor: Function, key: string) => void;
