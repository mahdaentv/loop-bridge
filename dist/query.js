import { EventEmitter } from './event';
export class Query {
    constructor(repository, filter = {}, options = {}) {
        this.repository = repository;
        this.filter = filter;
        this.options = options;
        this.events = new EventEmitter();
        Object.assign(this, { entities: [] });
        this.reset(filter);
    }
    get selectedEntities() {
        return this.entities.selected || [];
    }
    get fewEntitiesSelected() {
        return this.entities.fewSelected;
    }
    get allEntitiesSelected() {
        return this.entities.allSelected;
    }
    get prevEnabled() {
        return this.meta.page > 1;
    }
    get nextEnabled() {
        return this.meta.page < this.meta.totalPages;
    }
    reset(filter = {}) {
        this.filter = filter;
        this.params = {};
        this.meta = { page: 1, totalPages: 1, total: 0 };
        if (this.repository.configs.defaultLimit) {
            this.limit(this.repository.configs.defaultLimit);
        }
        if (!this.filter.limit) {
            this.filter.limit = 25;
        }
        return this;
    }
    assign(filter) {
        Object.assign(this.filter, filter);
        return this;
    }
    limit(limit, fetch, options) {
        this.filter.limit = limit;
        if (fetch) {
            this.fetch(options).then(() => { }).catch(() => { });
        }
        return this;
    }
    offset(offset, fetch, options) {
        this.filter.offset = offset;
        if (fetch) {
            this.fetch(options).then(() => { }).catch(() => { });
        }
        return this;
    }
    skip(skip, fetch, options) {
        this.filter.skip = skip;
        if (fetch) {
            this.fetch(options).then(() => { }).catch(() => { });
        }
        return this;
    }
    goto(page, fetch, options) {
        this.meta.page = page;
        this.filter.offset = this.filter.limit * (this.meta.page - 1);
        if (fetch) {
            this.fetch(options).then(() => { }).catch(() => { });
        }
        return this;
    }
    order(...orders) {
        if (typeof orders[0] === 'object') {
            this.filter.order = orders[0];
        }
        else {
            if (!this.filter.order) {
                this.filter.order = {};
            }
            for (const order of orders) {
                const [key, direction = 'ASC'] = order.split(' ');
                this.filter.order[key] = direction;
            }
        }
        return this;
    }
    where(where, fetch, options) {
        this.filter.where = where;
        if (fetch) {
            this.fetch(options).then(() => { }).catch(() => { });
        }
        return this;
    }
    include(include, fetch, options) {
        this.filter.include = include;
        if (fetch) {
            this.fetch(options).then(() => { }).catch(() => { });
        }
        return this;
    }
    fields(fields) {
        if (Array.isArray(fields)) {
            if (!this.filter.fields) {
                this.filter.fields = {};
            }
            for (const key of fields) {
                this.filter.fields[key] = true;
            }
        }
        else {
            this.filter.fields = fields;
        }
        return this;
    }
    set(key, value, fetch, options) {
        this.params[key] = value;
        if (fetch) {
            this.fetch(options).then(() => { }).catch(() => { });
        }
        return this;
    }
    rem(key, fetch, options) {
        delete this.params[key];
        if (fetch) {
            this.fetch(options).then(() => { }).catch(() => { });
        }
        return this;
    }
    prefix(path) {
        this.options.prefix = path;
        return this;
    }
    suffix(path) {
        this.options.suffix = path;
        return this;
    }
    toggleSelect(entity) {
        entity.selected = !entity.selected;
    }
    toggleSelectAll() {
        if (this.allEntitiesSelected) {
            this.entities.forEach(entity => entity.selected = false);
        }
        else {
            this.entities.forEach(entity => entity.selected = true);
        }
    }
    async subscribe(handler) {
        const { repository } = this;
        const { datasource } = repository;
        if (!this.subscription) {
            const filter = JSON.parse(JSON.stringify(this.filter));
            if (filter.order)
                filter.order = transformOrders(filter.order);
            const optn = JSON.parse(JSON.stringify({ filter }));
            this.subscription = await repository.subscribe(async (event) => {
                this.events.emit({ type: 'changes', data: event });
                const { data } = event;
                if (data.id) {
                    for (const entity of this.entities) {
                        if (data.id === entity.id) {
                            if (event.type === 'delete') {
                                entity.data.deletedAt = new Date();
                            }
                            else if (event.type === 'put' || event.type === 'patch') {
                                for (const [key, value] of Object.entries(data)) {
                                    entity.data[key] = value;
                                }
                            }
                        }
                    }
                }
                await this.refresh();
            }, optn);
        }
        if (handler) {
            return this.events.subscribe(handler);
        }
    }
    async unsubscribe() {
        if (this.subscription) {
            await this.subscription.unsubscribe();
            this.subscription = null;
        }
    }
    async count(options = {}) {
        if (this.filter.where)
            options.params = { where: this.filter.where };
        this.meta.total = await this.repository.count(options);
        this.meta.totalPages = Math.ceil((this.meta.total / this.filter.limit));
        return this;
    }
    async next(options) {
        this.goto(this.meta.page + 1, false, options);
        return await this.fetch(options);
    }
    async prev(options) {
        this.goto(this.meta.page - 1, false, options);
        return await this.fetch(options);
    }
    async fetch(options = {}) {
        this.state = 'loading';
        const filter = JSON.parse(JSON.stringify(this.filter));
        if (filter.order)
            filter.order = transformOrders(filter.order);
        try {
            const optn = JSON.parse(JSON.stringify(Object.assign(Object.assign(Object.assign({}, this.options), options), { filter, params: this.params })));
            const data = await this.repository.find(optn);
            this.entities = data;
            this.state = 'complete';
            if (this.subscription && !options.keepSubscription) {
                await this.unsubscribe();
                await this.subscribe();
            }
            return data;
        }
        catch (error) {
            throw error;
            this.state = 'complete';
        }
    }
    async more(options = {}) {
        this.state = 'load-more';
        const filter = JSON.parse(JSON.stringify(this.filter));
        if (filter.order)
            filter.order = transformOrders(filter.order);
        try {
            const offset = (this.filter.offset || 0) + this.filter.limit;
            const optn = JSON.parse(JSON.stringify(Object.assign(Object.assign(Object.assign({}, this.options), options), { filter: Object.assign(Object.assign({}, filter), { offset }), params: this.params })));
            const data = await this.repository.find(optn);
            if (data.length) {
                this.offset(offset);
                this.entities.push(...data);
            }
            this.state = 'complete';
            return data;
        }
        catch (error) {
            throw error;
            this.state = 'complete';
        }
    }
    async refresh() {
        await this.fetch({
            keepSubscription: true,
            force: true
        });
    }
    async update(id, body, options = {}) {
        return await this.repository.update(id, body, options);
    }
    async updateAll(body, options = {}) {
        const entities = options.selectedOnly ? this.selectedEntities : this.entities;
        return await this.repository.updateAll(entities, body, options);
    }
    async replace(id, body, options = {}) {
        return await this.repository.replace(id, body, options);
    }
    async delete(id, options = {}) {
        return await this.repository.delete(id, options);
    }
    async deleteAll(options = {}) {
        const entities = options.selectedOnly ? this.selectedEntities : this.entities;
        return await this.repository.deleteAll(entities, options);
    }
}
function transformOrders(orders) {
    return Object.keys(orders).map(key => `${key} ${orders[key]}`);
}
//# sourceMappingURL=query.js.map