export const dataSources = {
    default: null,
    sources: {}
};
export function repository(repository, datasource) {
    if (typeof repository === 'string' && repository.includes(':')) {
        const [d, r] = repository.split(':');
        repository = r;
        datasource = d;
    }
    return function (constructor, key) {
        Object.defineProperty(constructor, key, {
            get: () => getRepository(repository, datasource)
        });
    };
}
export function query(name, filter, options) {
    let repository, datasource;
    if (name.includes(':')) {
        const names = name.split(':');
        if (names.length === 3) {
            datasource = names[0];
            repository = names[1];
            name = names[2];
        }
        else {
            repository = names[0];
            name = names[1];
        }
    }
    else {
        throw new Error('Invalid query name format!');
    }
    if (!repository) {
        throw new Error(`Query need a repository name or type to be defined.`);
    }
    return function (constructor, key) {
        Object.defineProperty(constructor, key, {
            get: () => getRepository(repository, datasource).query(name, filter, options)
        });
    };
}
export function entity(repository, data) {
    return function (constructor, key) {
        const repo = getRepository(repository);
        constructor[key] = repo.createEntity(data);
    };
}
export function getRepository(repository, datasource) {
    let repo;
    if (datasource) {
        if (typeof datasource === 'string') {
            if (dataSources.sources[datasource]) {
                repo = dataSources.sources[datasource].getRepository(repository);
            }
            else {
                throw new Error(`Expected DataSource ${datasource} does not exist!`);
            }
        }
        else {
            let ds;
            for (const [, value] of Object.entries(dataSources.sources)) {
                if (value instanceof datasource) {
                    ds = value;
                }
            }
            if (ds) {
                repo = ds.getRepository(repository);
            }
            else {
                throw new Error(`Expected DataSource ${datasource} does not exist!`);
            }
        }
    }
    else {
        if (dataSources.default) {
            repo = dataSources.default.getRepository(repository);
        }
        else {
            if (Object.keys(dataSources.sources).length) {
                for (const [, value] of Object.entries(dataSources.sources)) {
                    if (!dataSources.default) {
                        dataSources.default = value;
                    }
                }
                repo = dataSources.default.getRepository(repository);
            }
            else {
                throw new Error(`Default DataSource does not exist!`);
            }
        }
    }
    if (repo) {
        return repo;
    }
    else {
        throw new Error(`Expected Repository ${repository} does not exist!`);
    }
}
export function Schema(schemas) {
    return function (constructor, key) {
        constructor.prototype.schemas = schemas;
    };
}
//# sourceMappingURL=decorator.js.map