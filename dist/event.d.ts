import { RequestEvent } from './interfaces';
export declare type EventHandler<T> = (event: RequestEvent<T>) => void;
export declare type Unsubscriber = () => void;
export declare class EventEmitter<T> {
    listeners: EventHandler<T>[];
    emit(event: RequestEvent<T>): void;
    subscribe(handler: EventHandler<T>): Unsubscriber;
    unsubscribe(handler: EventHandler<T>): void;
}
