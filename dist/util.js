import * as _ from 'lodash';
export function diff(nextRec, prevRec) {
    return getChanges(nextRec, prevRec);
}
function getChanges(next, prev) {
    return _.transform(next, (result, value, key) => {
        if (!_.isEqual(value, prev[key])) {
            if (_.isObject(value) && _.isObject(prev[key])) {
                result[key] = getChanges(value, prev[key]);
            }
            else {
                result[key] = value;
            }
        }
    });
}
//# sourceMappingURL=util.js.map