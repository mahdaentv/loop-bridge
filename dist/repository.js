import { Entities, Entity } from './entity';
import { EventEmitter } from './event';
import { Query } from './query';
import { SchemaError } from './schema';
export class Repository {
    constructor(datasource, configs) {
        this.datasource = datasource;
        this.configs = configs;
        this.queries = {};
        this.events = new EventEmitter();
        this.EntityClass = Entity;
        this.datasource.addRepository(this);
        if (configs.schemas) {
            this.schemas = configs.schemas;
        }
    }
    get endpoint() {
        return this.configs.endpoint;
    }
    query(name, filter, options) {
        if (typeof name === 'string') {
            if (!this.queries[name]) {
                this.queries[name] = new Query(this, filter, options);
            }
            return this.queries[name];
        }
        else {
            return new Query(this, name, options);
        }
    }
    async count(options = {}) {
        try {
            this.prefixRelations(options);
            const response = await this.datasource.get(`${this.endpoint}/count`, options);
            return parseFloat(response.data.count);
        }
        catch (error) {
            return 0;
        }
    }
    async find(options = {}) {
        try {
            if (this.beforeFind)
                await this.beforeFind(options);
            this.prefixRelations(options);
            this.injectHeaders(options);
            const response = await this.datasource.get(this.endpoint, options);
            if (this.transformFindResponse)
                this.transformFindResponse(response);
            const entities = this.createEntity(response.data, options);
            if (options && (options.withChildren || options.recursive)) {
                await this.injectRelations(entities, options.recursive);
            }
            if (this.afterFind)
                await this.afterFind(entities, response);
            this.events.emit({ type: 'find', options, entities, response });
            return entities;
        }
        catch (error) {
            if (this.failedFind)
                await this.failedFind(error, options);
            throw error;
        }
    }
    async findOne(id, options = {}) {
        try {
            if (this.beforeFindOne)
                await this.beforeFindOne(id, options);
            this.prefixRelations(options);
            this.injectHeaders(options);
            const response = await this.datasource.get(`${this.endpoint}/${id}`, options);
            if (this.transformFindOneResponse)
                this.transformFindOneResponse(response);
            const entity = this.createEntity(response.data, options);
            if (options && (options.withChildren || options.recursive)) {
                await this.injectRelations(entity, options.recursive);
            }
            if (this.afterFindOne)
                await this.afterFindOne(entity, response);
            this.events.emit({ type: 'find-one', id, options, entity, response });
            return entity;
        }
        catch (error) {
            if (this.failedFindOne)
                await this.failedFindOne(error, id, options);
            throw error;
        }
    }
    async create(body, options = {}) {
        try {
            if (this.beforeCreate)
                await this.beforeCreate(body, options);
            this.excludeFields(body, options);
            this.prefixRelations(options, body);
            this.ensureSchemas(body);
            this.injectHeaders(options);
            const response = await this.datasource.post(this.endpoint, body, options);
            if (this.transformCreateResponse)
                this.transformCreateResponse(response);
            const entity = this.createEntity(response.data, options);
            if (this.afterCreate)
                await this.afterCreate(entity, response);
            this.events.emit({ type: 'create', options, entity, response });
            return entity;
        }
        catch (error) {
            if (this.failedCreate)
                await this.failedCreate(error, body, options);
            throw error;
        }
    }
    async update(id, body, options = {}) {
        try {
            if (this.beforeUpdate)
                await this.beforeUpdate(id, body, options);
            this.excludeFields(body, options);
            this.injectHeaders(options);
            const response = await this.datasource.patch(`${this.endpoint}/${id}`, body, options);
            if (this.transformUpdateResponse)
                this.transformUpdateResponse(response);
            if (this.afterUpdate)
                await this.afterUpdate(response);
            this.events.emit({ type: 'update', id, options, response });
        }
        catch (error) {
            if (this.failedUpdate)
                await this.failedUpdate(error, id, body, options);
            throw error;
        }
    }
    async updateAll(entities, body, options = {}) {
        try {
            this.excludeFields(body, options);
            this.ensureSchemas(body);
            this.injectHeaders(options);
            if (this.configs.hasBulkOperations) {
                const ids = entities.map(entity => entity.id);
                const response = await this.datasource.patch(`${this.endpoint}?entityIds=${ids.toString()}`, body, options);
                entities.forEach(entity => entity.assign(body));
                this.events.emit({ type: 'update-all', options, response });
            }
            else {
                const responses = await Promise.all(entities.map(entity => entity.update(body, options)));
                this.events.emit({ type: 'update-all', options, data: responses });
            }
        }
        catch (error) {
            throw error;
        }
    }
    async replace(id, body, options = {}) {
        try {
            if (this.beforeReplace)
                await this.beforeReplace(id, body, options);
            this.excludeFields(body, options);
            this.ensureSchemas(body);
            this.injectHeaders(options);
            const response = await this.datasource.put(`${this.endpoint}/${id}`, body, options);
            if (this.transformReplaceResponse)
                this.transformReplaceResponse(response);
            if (this.afterReplace)
                await this.afterReplace(response);
            this.events.emit({ type: 'replace', id, options, response });
        }
        catch (error) {
            if (this.failedReplace)
                await this.failedReplace(error, id, body, options);
            throw error;
        }
    }
    async delete(id, options = {}) {
        try {
            if (this.beforeDelete)
                await this.beforeDelete(id, options);
            this.injectHeaders(options);
            const response = await this.datasource.delete(`${this.endpoint}/${id}`, options);
            if (this.transformDeleteResponse)
                this.transformDeleteResponse(response);
            if (this.afterDelete)
                await this.afterDelete(response);
            this.events.emit({ type: 'delete', id, options, response });
        }
        catch (error) {
            if (this.failedDelete)
                await this.failedDelete(error, id, options);
            throw error;
        }
    }
    async deleteAll(entities, options = {}) {
        try {
            this.injectHeaders(options);
            if (this.configs.hasBulkOperations) {
                const ids = entities.map(entity => entity.id);
                const response = await this.datasource.delete(`${this.endpoint}?entityIds=${ids.toString()}`, options);
                entities.forEach(entity => entity.assign({ deletedAt: new Date() }));
                this.events.emit({ type: 'delete-all', options, response });
            }
            else {
                const responses = await Promise.all(entities.map(entity => entity.delete(options)));
                this.events.emit({ type: 'delete-all', options, data: responses });
            }
        }
        catch (error) {
            throw error;
        }
    }
    async subscribe(idHandler, optionsHandler, options) {
        if (typeof idHandler === 'string') {
            await this.mergeEndpoint(options);
            await this.injectHeaders(options);
            return await this.datasource.subscribe(`${this.endpoint}/${idHandler}`, optionsHandler, options);
        }
        else {
            await this.mergeEndpoint(optionsHandler);
            await this.injectHeaders(optionsHandler);
            return await this.datasource.subscribe(this.endpoint, idHandler, optionsHandler);
        }
    }
    excludeFields(body, options) {
        if (Array.isArray(options.excludeFields)) {
            for (const key of options.excludeFields) {
                delete body[key];
            }
        }
    }
    createEntity(data, options) {
        if (!data) {
            return new this.EntityClass(this, {});
        }
        if (Array.isArray(data)) {
            const entities = new Entities(this, ...data.map(item => {
                if (item) {
                    this.ensureSchemas(item, true);
                }
                return new this.EntityClass(this, item, options);
            }));
            if (data.hasOwnProperty('meta')) {
                entities.meta = data.meta;
            }
            return entities;
        }
        else {
            if (data) {
                this.ensureSchemas(data, true);
            }
            return new this.EntityClass(this, data, options);
        }
    }
    ensureSchemas(data, remote) {
        if (this.schemas) {
            const errContext = {};
            for (const [key, definition] of Object.entries(this.schemas)) {
                if (data.hasOwnProperty(key)) {
                    const givenType = toString.call(data[key])
                        .replace('[object ', '')
                        .replace(']', '')
                        .toLowerCase();
                    if (definition.type !== givenType) {
                        errContext[key] = { expected: definition.type, given: data[key] };
                    }
                }
                else {
                    if (definition.default) {
                        if (typeof definition.default === 'function') {
                            data[key] = definition.default(key);
                        }
                        else {
                            data[key] = definition.default;
                        }
                    }
                    else {
                        if (remote) {
                            if (definition.required) {
                                errContext[key] = { expected: definition.type, given: undefined };
                            }
                        }
                        else {
                            if (definition.required && !definition.generated) {
                                errContext[key] = { expected: definition.type, given: undefined };
                            }
                        }
                    }
                }
            }
            if (Object.keys(errContext).length) {
                throw new SchemaError(errContext);
            }
        }
    }
    useClass(type) {
        this.EntityClass = type;
    }
    async injectRelations(data, recursive) {
        if (data instanceof Entities) {
            await Promise.all(data.map(item => this.injectRelations(data)));
        }
        else {
            const promises = [];
            const { relations } = this.configs;
            if (relations && relations.hasMany) {
                for (const rel of relations.hasMany) {
                    const instance = this.datasource.getRepository(rel.repository);
                    if (instance) {
                        promises.push(new Promise(async (resolve, reject) => {
                            try {
                                const child = await instance.find({
                                    params: { [rel.foreignKey]: data['id'] },
                                    recursive
                                });
                                data[rel.localField] = child;
                                resolve(data);
                            }
                            catch (error) {
                                reject(error);
                            }
                        }));
                    }
                }
            }
            if (relations && relations.hasOne) {
                for (const rel of relations.hasOne) {
                    if (data[rel.foreignKey]) {
                        const instance = this.datasource.getRepository(rel.repository);
                        if (instance) {
                            promises.push(new Promise(async (resolve, reject) => {
                                try {
                                    const child = await instance.findOne(data[rel.foreignKey], { recursive });
                                    data[rel.localField] = child;
                                    resolve(data);
                                }
                                catch (error) {
                                    reject(error);
                                }
                            }));
                        }
                    }
                }
            }
            if (promises.length) {
                await Promise.all(promises);
            }
        }
        return data;
    }
    prefixRelations(options = {}, body) {
        const relationRefs = this.parseRelations(options, body);
        if (relationRefs.length) {
            const prefixes = relationRefs.map(({ key, endpoint, value }) => {
                delete options.params[key];
                delete (body || {})[key];
                return [endpoint, value].join('/');
            });
            options.prefix = options.prefix ? `${prefixes.join('/')}/${options.prefix}` : prefixes.join('/');
        }
        this.mergeEndpoint(options);
    }
    parseRelations(options = {}, body) {
        const { relations } = this.configs;
        const { params = {} } = options;
        if (relations && relations.belongsTo) {
            const relationRefs = [];
            const relationParams = {};
            for (const rel of relations.belongsTo) {
                const { repository, foreignKey } = rel;
                const instance = this.datasource.getRepository(repository);
                if (instance) {
                    const { endpoint } = instance.configs;
                    if (body && body[foreignKey]) {
                        relationRefs.push({
                            key: foreignKey,
                            value: body[foreignKey],
                            endpoint
                        });
                        relationParams[foreignKey] = body[foreignKey];
                    }
                    if (params[foreignKey]) {
                        relationRefs.push({
                            key: foreignKey,
                            value: params[foreignKey],
                            endpoint
                        });
                        relationParams[foreignKey] = params[foreignKey];
                    }
                }
            }
            if (relationRefs.length) {
                if (!options.params) {
                    options.params = {};
                }
                options.params.relationRef = relationParams;
            }
            return relationRefs;
        }
        return [];
    }
    mergeEndpoint(options = {}) {
        const { endpointPrefix, endpointSuffix } = this.configs;
        const { prefix, suffix } = options;
        if (endpointPrefix) {
            options.prefix = prefix ? `${endpointPrefix}/${prefix}` : endpointPrefix;
        }
        if (endpointSuffix) {
            options.suffix = suffix ? `${endpointSuffix}/${suffix}` : endpointSuffix;
        }
    }
    injectHeaders(options = {}) {
        if (this.configs.headers) {
            options.headers = Object.assign(Object.assign({}, this.configs.headers), options.headers || {});
        }
    }
}
//# sourceMappingURL=repository.js.map