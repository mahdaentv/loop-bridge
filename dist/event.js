export class EventEmitter {
    constructor() {
        this.listeners = [];
    }
    emit(event) {
        for (const handler of this.listeners) {
            if (typeof handler === 'function') {
                handler(event);
            }
        }
    }
    subscribe(handler) {
        if (Array.isArray(this.listeners)) {
            if (typeof handler === 'function' && !this.listeners.includes(handler)) {
                this.listeners.push(handler);
                return () => this.unsubscribe(handler);
            }
        }
    }
    unsubscribe(handler) {
        if (Array.isArray(this.listeners)) {
            this.listeners.splice(this.listeners.indexOf(handler), 1);
        }
    }
}
//# sourceMappingURL=event.js.map