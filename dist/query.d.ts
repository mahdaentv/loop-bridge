import { EntityData, EntityPart, FieldList, Fields, InclusionFilter, OrderFilter, QueryMetadata, RequestFilter, RequestOptions, RequestParams, WhereFilter } from './interfaces';
import { Repository } from './repository';
import { Entities, Entity } from './entity';
import { EventEmitter, EventHandler, Unsubscriber } from './event';
export declare class Query<T extends EntityData> {
    private repository;
    filter: RequestFilter<T>;
    options: RequestOptions<T>;
    private subscription;
    events: EventEmitter<unknown>;
    entities: Entities<T>;
    params: RequestParams;
    meta: QueryMetadata;
    state: 'loading' | 'load-more' | 'complete';
    get selectedEntities(): Entity<T>[];
    get fewEntitiesSelected(): boolean;
    get allEntitiesSelected(): boolean;
    get prevEnabled(): boolean;
    get nextEnabled(): boolean;
    constructor(repository: Repository<T>, filter?: RequestFilter<T>, options?: RequestOptions<T>);
    reset(filter?: RequestFilter<T>): this;
    assign(filter: RequestFilter<T>): this;
    limit(limit: number, fetch?: boolean, options?: RequestOptions<T>): this;
    offset(offset: number, fetch?: boolean, options?: RequestOptions<T>): this;
    skip(skip: number, fetch?: boolean, options?: RequestOptions<T>): this;
    goto(page: number, fetch?: boolean, options?: RequestOptions<T>): this;
    order(orders: OrderFilter<T>): this;
    order(...orders: string[]): this;
    where(where: WhereFilter<T> | WhereFilter<T>[], fetch?: boolean, options?: RequestOptions<T>): this;
    include(include: InclusionFilter | InclusionFilter[], fetch?: boolean, options?: RequestOptions<T>): this;
    fields(fields: FieldList<T> | Fields<T>): this;
    set(key: string, value: any, fetch?: boolean, options?: RequestOptions<T>): this;
    rem(key: string, fetch?: boolean, options?: RequestOptions<T>): this;
    prefix(path: string): this;
    suffix(path: string): this;
    toggleSelect(entity: Entity<T>): void;
    toggleSelectAll(): void;
    subscribe(handler?: EventHandler<T>): Promise<Unsubscriber | void>;
    unsubscribe(): Promise<void>;
    count(options?: RequestOptions<T>): Promise<this>;
    next(options?: RequestOptions<T>): Promise<Entity<T>[]>;
    prev(options?: RequestOptions<T>): Promise<Entity<T>[]>;
    fetch(options?: RequestOptions<T>): Promise<Entities<T>>;
    more(options?: RequestOptions<T>): Promise<Entities<T>>;
    refresh(): Promise<void>;
    update(id: string, body: EntityPart<T>, options?: RequestOptions<T>): Promise<void>;
    updateAll(body: EntityPart<T>, options?: RequestOptions<T>): Promise<void>;
    replace(id: string, body: T, options?: RequestOptions<T>): Promise<void>;
    delete(id: string, options?: RequestOptions<T>): Promise<void>;
    deleteAll(options?: RequestOptions<T>): Promise<void>;
}
