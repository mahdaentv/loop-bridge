import axios from 'axios/dist/axios';
import { dataSources } from './decorator';
import { Client, stringify } from 'event-bridge/dist/client';
const dateRegEx = /^[\d]{4}-[\d]{2}-[\d2]{2}T[\d]{2}:[\d]{2}:[\d]{2}[.\d]?Z$/;
const intRegRex = /^[\d.]+$/;
if (!window.DSCache) {
    window.DSCache = {};
}
export class DataSource {
    constructor(configs) {
        this.configs = configs;
        this.repositories = [];
        this.caches = window.DSCache;
        this.defaultHeaders = {
            'Accept': '*/*',
            'Content-Type': 'application/json'
        };
        this.customHeaders = {};
        this.http = axios.create({ baseURL: configs.baseURL });
        this.wstp = new Client(configs);
        this.connected = this.wstp.connected;
        this.disconnected = this.wstp.disconnected;
        this.message = this.wstp.message;
        this.connected.subscribe(() => this.readyState = this.wstp.readyState);
        if (configs.headers) {
            Object.assign(this.defaultHeaders, configs.headers);
        }
        dataSources.sources[configs.name] = this;
        if (configs.default) {
            dataSources.default = this;
        }
    }
    get headers() {
        return Object.assign(Object.assign({}, this.defaultHeaders), this.customHeaders);
    }
    connect(reconnect) {
        this.wstp.connect(reconnect);
    }
    async get(path, options = {}) {
        return await this.request({ method: 'GET', path, options });
    }
    async post(path, body, options = {}) {
        return await this.request({ method: 'POST', path, options, body });
    }
    async patch(path, body, options = {}) {
        return await this.request({ method: 'PATCH', path, options, body });
    }
    async put(path, body, options = {}) {
        return await this.request({ method: 'PUT', path, options, body });
    }
    async delete(path, options = {}) {
        return await this.request({ method: 'DELETE', path, options });
    }
    async subscribe(path, handler, options = {}) {
        const { headers = {}, prefix, suffix } = options;
        if (prefix) {
            path = `${prefix}/${path}`;
        }
        if (suffix) {
            path = `${path}/${suffix}`;
        }
        path = path.replace(/[\/]+/g, '/');
        return await this.wstp.subscribe(path, (event) => {
            event.data = this.transformData(event.data || {});
            return handler(event);
        }, Object.assign(Object.assign({}, options), { headers: Object.assign(Object.assign({}, this.headers), headers) }));
    }
    addRepository(instance) {
        if (!this.repositories.includes(instance)) {
            this.repositories.push(instance);
        }
    }
    getRepository(type) {
        if (typeof type === 'string') {
            for (const instance of this.repositories) {
                if (instance.configs.name === type) {
                    return instance;
                }
            }
        }
        else {
            for (const instance of this.repositories) {
                if (instance instanceof type) {
                    return instance;
                }
            }
        }
    }
    setHeader(key, value) {
        this.customHeaders[key] = value;
    }
    remHeader(key) {
        delete this.customHeaders[key];
    }
    clearCache() {
        this.caches = {};
    }
    async request({ method, path, options, body }) {
        try {
            const { cacheRequests, cacheAge } = this.configs;
            const cacheKey = JSON.stringify({ method, path, options });
            if (method === 'GET' && cacheRequests && this.caches[cacheKey] && !options.force) {
                const cache = this.caches[cacheKey] ? JSON.parse(this.caches[cacheKey]) : this.caches[cacheKey];
                if (cache.cacheAge || cacheAge) {
                    const rdiff = (new Date().getTime() - cache.createdAt.getTime());
                    if (cache.cacheAge) {
                        if (rdiff < cache.cacheAge) {
                            this.transformData(cache.response.data);
                            return cache.response;
                        }
                    }
                    else {
                        if (rdiff < cacheAge) {
                            this.transformData(cache.response.data);
                            return cache.response;
                        }
                    }
                }
                else {
                    this.transformData(cache.response.data);
                    return cache.response;
                }
            }
            if (this.beforeRequest)
                await this.beforeRequest({ method, path, body, options });
            const request = this.createRequest(path, options);
            try {
                const config = {
                    url: request.path,
                    headers: request.headers,
                    method: method.toLowerCase()
                };
                if (body)
                    config.data = body;
                const { protocol = this.configs.socketFirst ? 'ws' : 'http' } = options || {};
                let response;
                if (protocol === 'ws' && this.wstp.readyState === WebSocket.OPEN) {
                    response = await this.wstp.request(config);
                }
                else {
                    response = await this.http.request(config);
                }
                if (this.transformResponse)
                    await this.transformResponse(response, request, method);
                if (typeof options.onSuccess === 'function') {
                    await options.onSuccess(response);
                }
                this.transformData(response.data);
                if (this.afterRequest)
                    await this.afterRequest({ method, request, response });
                if (method === 'GET' && cacheRequests) {
                    const cache = { response, createdAt: new Date() };
                    if (options.cacheAge) {
                        cache.cacheAge = options.cacheAge;
                    }
                    this.caches[cacheKey] = JSON.stringify(cache);
                }
                return response;
            }
            catch (error) {
                if (this.failedRequest)
                    await this.failedRequest({ method, request, error });
                if (typeof options.onError === 'function') {
                    await options.onError(error);
                }
                throw error;
            }
        }
        catch (error) {
            if (this.failedRequest)
                await this.failedRequest({ method, error });
            if (typeof options.onError === 'function') {
                await options.onError(error);
            }
            throw error;
        }
    }
    createRequest(path, { params, filter, headers, prefix, suffix } = {}) {
        const request = { path, headers: this.headers };
        const queries = [];
        if (params && Object.keys(params).length) {
            queries.push(stringify(params));
        }
        if (filter) {
            queries.push(`filter=${JSON.stringify(filter)}`);
        }
        if (headers) {
            request.headers = Object.assign(Object.assign({}, request.headers), headers);
        }
        if (suffix) {
            request.path = `${request.path}/${suffix}`;
        }
        if (queries.length) {
            request.path = `${request.path}?${queries.join('&')}`;
        }
        if (prefix) {
            request.path = `${prefix}/${request.path}`;
        }
        request.path = request.path.replace(/[\/]+/g, '/');
        return request;
    }
    transformData(data) {
        if (Array.isArray(data)) {
            return data.map(item => this.transformData(item));
        }
        else if (toString.call(data) === '[object Object]') {
            const { transformTypes } = this.configs;
            const toDate = Array.isArray(transformTypes) && transformTypes.includes('date');
            const toNumber = Array.isArray(transformTypes) && transformTypes.includes('number');
            const toBoolean = Array.isArray(transformTypes) && transformTypes.includes('boolean');
            const toNull = Array.isArray(transformTypes) && transformTypes.includes('null');
            for (const [key, value] of Object.entries(data)) {
                if (toDate && dateRegEx.test(value)) {
                    data[key] = new Date(value);
                }
                if (toNull && value === 'null') {
                    data[key] = null;
                }
                if (toBoolean && value === 'true') {
                    data[key] = true;
                }
                if (toBoolean && value === 'false') {
                    data[key] = false;
                }
                if (toNumber && intRegRex.test(value)) {
                    data[key] = parseFloat(value);
                }
                if (Array.isArray(value) || toString.call(value) === '[object Object]') {
                    this.transformData(value);
                }
            }
            return data;
        }
        else {
            return data;
        }
    }
}
//# sourceMappingURL=datasource.js.map