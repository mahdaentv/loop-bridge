export interface SchemaDefinitions {
    [key: string]: SchemaDefinition;
}
export interface SchemaDefinition {
    type: 'string' | 'date' | 'number' | 'object' | 'array' | 'boolean';
    required?: boolean;
    generated?: boolean;
    default?: any | valueMaker;
}
export declare type valueMaker = (data: any) => any;
export declare class SchemaError extends Error {
    context: SchemaErrorContext;
    constructor(context: SchemaErrorContext);
}
export interface SchemaErrorContext {
    [key: string]: {
        expected: string;
        given: any;
    };
}
