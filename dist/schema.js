export class SchemaError extends Error {
    constructor(context) {
        super(`Schema Error: ${JSON.stringify(context)}`);
        this.context = context;
    }
}
//# sourceMappingURL=schema.js.map