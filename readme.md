An OpenAPI Client Library for Javascript.

# Getting Started

## Installation
To start using LoopBridge, add it to your package.

```bash
npm install --save loop-bridge
```

## Creating DataSource
DataSource is the first thing you need to create to
start using LoopBridge.

```typescript
import { DataSource, DataSourceConfigs } from 'loop-bridge';

export const dsConfig: DataSourceConfigs = {
  name: 'main',
  baseURL: 'http://localhost:3000'
};

export const dataSource = new DataSource(dsConfig);
```

If you want to use the DataSource as an Angular Service, you
can simply create a service that extends the DataSource.

**Angular Service**
```typescript
@Injectable({ providedIn: 'root' })
export class DataSourceService extends DataSource {
  constructor() {
    super(dsConfig);
  }
}
```

## Creating Repository
After creating DataSource, you will need to create
a Repository to manage an endpoint. We prefer to create
a subclass for a detailed typings, and work with decorators.

```typescript
import { Repository, RepositoryConfigs, EntityData } from 'loop-bridge';
import { dataSource } from './datasource';

export const userConfigs: RepositoryConfigs = {
  name: 'user',
  endpoint: '/users'
};

export interface User extends EntityData {
  fisrtName: string;
  lastName: string;
  email: string;
};

export class Users extends Repository<User> {}

export const users = new Users(dataSource, userConfigs);
```

**Angular Service**
```typescript
@Injectable({ providedIn: 'root' })
export class Users extends Repository<User> {
  constructor(source: DataSourceService) {
    super(source, userConfigs);
  }
}
```

## Requests
After creating DataSource and Repository, now
you can start requesting data from the API.

### List all Entities
To list a data of an endpoint, you can use the `.find()`
method. This method will return an Entities object, a subclass
of an Array with Entity as its content.

```typescript
users.find().then(entities => {
  for (const entity of entities) {
    console.log(entity.data.email);
  }
});
```

### Get single Entity.
To get a single Entity, you can use the `.findOne()` method.
This method will return an Entity object that contains the entity data and
methods to manage the entity data.

```typescript
users.findOne('1').then(entity => {
  console.log(`User ${entity.data.email} is createdAt ${entity.createdAt}`);
});
```

### Create an Entity
To create an Entity, you can use the `.create()` method
that will do a `POST` request. You can also
create a blank Entity if you want to use it as a form
using `.createEntity()` method.

The difference is, `.create()` method will do a
`POST` request directly and return an Entity object,
while `.createEntity()` will simply return an Entity
object without doing any request.

**Direct Request**
```typescript
users.create({ firstName: 'John', lastName: 'Doe', email: 'john@domain.com' })
  .then(entity => {
    console.log(entity.createdAt);
  });
```

**Blank Entity**
```typescript
const entity = users.createEntity();

entity.data.firstName = 'John';
entity.data.lastName = 'Doe';
entity.data.email = 'john@domain.com';

entity.save().then(() => {
  console.log(entity.createdAt);
});
```

### Update an Entity
To update an Entity, you can use the `repository.update()`
method or `repository.replace()` method. You can also
update the data directy from the Entity object using
the `entity.save()` method or `entity.update()` method.

The difference is, when using repository method,
you need to specify the `Entity ID` and the changes
wont be applied to the existing `Entity Object` directly.
While using entity method, the changes are applied to
the Entity data after the request complete.

* Use `repository.update()` method to update the partial data. It will do a `PATCH` request to the API.
* Use `repository.replace()` method to replace the entire data. It will do a `PUT` request to the API.
* Use `entity.save()` or `entity.update()` to update the partial data. It will do a `PATCH` request to the API.

**Using Repository Method**
```typescript
users.update('1', { firstName: 'Johnny' }).then(console.log);
```

**Using Entity Method**
```typescript
entity.data.firstName = 'Johnny';
entity.save().then(console.log);
```

### Delete an Entity
To delete an Entity, you can use the `repository.delete()` method
or `entity.delete()` method. The difference is, if using
the repository method, the existing Entity will not get the `deletedAt` property,
while using the entity method it will automatically add
a `deletedAt` property to the Entity object.

The `deletedAt` property will be useful if you don't
want to redirect the page to another page after deleting
an Entity so you can display a deleted info.

**Using Repository Method**
```typescript
users.delete('1').then(console.log);
```

**Using Entity Method**
```typescript
entity.delete().then(console.log);
```

### Bulk Requests
Sometimes you want to manage all Entities at once. To do that,
you can use an Entities method to update or delete all
entities inside that.

By default it will do a lot requests to the API. If your
API support bulk operations, then you can specify
`{ hasBulkOperations: true }` to the repository config, so
it will do a single `PATCH /endpoint?entityIds=[]` request.

**Using Repository Method**
```typescript
users.deleteAll(entities).then(console.log);
```

**Using Entities Method**
```typescript
entities.delete().then(console.log);
```

## Schema
Schema will help you to validate the incoming and outgoing request body,
to do a validation at the client level. To define a schema, you can
add that to the RepositoryConfigs or using decorator.

**Using Configs**
```typescript
export const userConfigs: RepositoryConfigs = {
  schemas: {
    firstName: { type: 'string', required: true }
  }
};
```

**Using Decorator**
```typescript
import { Schema, Repository } from 'loop-bridge';

@Schema({
  firstName: { type: 'string', required: true }
})
export class Users extends Repository<User> {

}
```

**Validating Form**
```typescript
export class UserFormComponent {
  @entity(Users)
  form: Entity<User>;

  async submit() {
    if (this.form.valid) {
      this.form.save();
    }
  }
}
```

## Caching
Caching will be useful to reduce the requests to the API
because the DataSource will return the cached request instead re-requesting from the API.

### Enable Caching
To enable caching, add the `{ cacheRequests: true }` to the
DataSource config.

```typescript
export const dsConfigs: DataSourceConfigs = {
  cacheRequests: true
};
```

### Set the Cache age
You can also set the cache age, so DataSource will re-request from the API if the cached
request is expired. You can define the cache age at the DataSource level, repository level, even
at the request level. Cache age is in `ms`.

**DataSource Level**
```typescript
export const dsConfigs: DataSourceConfigs = {
  cacheRequests: true,
  cacheAge: 10000
};
```

**Repository Level**
```typescript
export const userConfigs: RepositoryConfigs = {
  cacheAge: 5000
};
```

**Request Level**
```typescript
users.find({ cacheAge: 3000 }).then(console.log);
```

### Bypass the cache.
Sometimes you want to bypass the cache so it will re-request from the API. To do that, add the `{ force: true }`
to the request options.

```typescript
users.find({ force: true }).then(console.log);
```

## Relation
Repository can have a `belongsTo`, `hasOne`, and `hasMany` relations.

* Use `belongsTo` relation to prefix the request with the parent repository endpoint.
* Use `hasOne` to transform a field in an Entity with another Entity type.
* Use `hasMany` to tranform a field in an Eneity with antother Entities type.

### Belongs To Relation
Belongs To relation will be useful to automatically prefix the endpoint with the parent endpoint.
For example, if you want to add a `project` to a `user`, then the `project` repository is belongs to
`user` repository, and the endpoint to create a project is `/users/:user_id/projects`.

Without an automated relation, you need to manually prefix the request like `proejcts.create({ prefix: '/users/1' })`.
With an automated relation, you just need to specify the `foreignKey`.


```typescript
import { Repository, RepositoryConfigs } from 'loop-bridge';

export class Users extends Repository<User> {}
export class Projects extends Repository<Project> {}

export const projectConfigs: RepositoryConfigs = {
  endpoint: '/projects',
  relations: {
    belongsTo: [{ repository: Users, foreginKey: 'userId', localField:'user' }]
  }
};

const projects = new Projects(datasource, projectConfigs);
projects.create({ name:'Test', userId: '1' }); // POST /users/1/projects

``` 

### Has One and Has Many Relation.
Has One and Has Many relation will transform a field of an Entity into the target repository Entity/Entities.
This relation also can help you to automatically fetch the child data in a single method if the API doesn't
support `include` filter.

To automatically fetch the child data, the child Repository musth have `belongsTo` relation defined, and
add `{ withChildren: true }` to the request options. You can also add `{ recursive: true }` to the
request options to automatically fetch the child data recursively.

```typescript
export const userConfigs: RepositoryConfigs = {
  relations: {
    hasMany: [{ repository: Projects, localField: 'projects', foreignKey: 'userId' }]
  }
};
users.findOne('1', { withChildren: true }).then(entity => console.log(entity.data.projects)); 
```
